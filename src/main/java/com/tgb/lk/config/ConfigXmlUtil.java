package com.tgb.lk.config;

import com.tgb.lk.util.AppConstants;
import com.tgb.lk.util.XmlUtil;

public class ConfigXmlUtil extends XmlUtil<Config> {
	private static String file = AppConstants.BASE_PATH + ".config/config.xml";

	public ConfigXmlUtil() {
		super(Config.class, file);
	}

	@Override
	public Config setDefaultConfig() {
		Config config = new Config();
		config.setTemplatesDir(AppConstants.BASE_PATH + "templates-var");
		config.setFixedFilesDir(AppConstants.BASE_PATH + "templates-fix");
		config.setBeansDir(AppConstants.BASE_PATH + "dest-beans");
		config.setLibsDir(AppConstants.BASE_PATH + ".libs");
		config.setDestCodeDir(AppConstants.BASE_PATH + "dest-code");
		config.setCtxConfig("author=李坤\n" +
				"version=1.0.0\n" +
				"base-package=com.tgb.lk");
		config.setChkDBSource(false);
		return config;
	}

	public static void main(String[] args) {
		ConfigXmlUtil util = new ConfigXmlUtil();
		System.out.println(util.getConfig());
	}

}
