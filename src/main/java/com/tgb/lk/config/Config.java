package com.tgb.lk.config;

import java.io.File;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("config")
public class Config {
	@XStreamAlias("templates-dir")
	private String templatesDir;

	@XStreamAlias("fixed-files-dir")
	private String fixedFilesDir;

	@XStreamAlias("libs-dir")
	private String libsDir;

	@XStreamAlias("beans-dir")
	private String beansDir;

	@XStreamAlias("beans-package")
	private String beansPackage;

	@XStreamAlias("dest-code-dir")
	private String destCodeDir;

	@XStreamAlias("ctx-config")
	private String ctxConfig;

	@XStreamAlias("chk-db-source")
	private boolean chkDBSource;

	public String filterDir(String dirPath) {
		return dirPath.replace("/", File.separator).replace("\\",
				File.separator);
	}

	public String getTemplatesDir() {
		return filterDir(templatesDir);
	}

	public void setTemplatesDir(String templatesDir) {
		this.templatesDir = templatesDir;
	}

	public String getFixedFilesDir() {
		return filterDir(fixedFilesDir);
	}

	public void setFixedFilesDir(String fixedFilesDir) {
		this.fixedFilesDir = fixedFilesDir;
	}

	public String getLibsDir() {
		return filterDir(libsDir);
	}

	public void setLibsDir(String libsDir) {
		this.libsDir = libsDir;
	}

	public String getBeansDir() {
		return filterDir(beansDir);
	}

	public void setBeansDir(String beansDir) {
		this.beansDir = beansDir;
	}

	public String getDestCodeDir() {
		return filterDir(destCodeDir);
	}

	public void setDestCodeDir(String destCodeDir) {
		this.destCodeDir = destCodeDir;
	}

	public String getBeansPackage() {
		return beansPackage;
	}

	public void setBeansPackage(String beansPackage) {
		this.beansPackage = beansPackage;
	}

	public String getCtxConfig() {
		return ctxConfig;
	}

	public void setCtxConfig(String ctxConfig) {
		this.ctxConfig = ctxConfig;
	}

	public boolean isChkDBSource() {
		return chkDBSource;
	}

	public void setChkDBSource(boolean chkDBSource) {
		this.chkDBSource = chkDBSource;
	}

}
