package com.tgb.lk.ui;

import java.awt.*;
import javax.swing.*;

import com.tgb.lk.table.Log;
import com.tgb.lk.util.AppConstants;

public class About extends JDialog {
	private static final long serialVersionUID = 1L;

	public About(Frame frame, String title, boolean modal) {
		super(frame, title, modal);
		panel1 = new JPanel();
		jLabel1 = new JLabel();
		layOut = new GridBagLayout();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();
		jLabel4 = new JLabel();
		jLabel5 = new JLabel();
		jLabel6 = new JLabel();
		jLabel7 = new JLabel();
		jEditorPane = new JEditorPane();
		try {
			jbInit();
			pack();
		} catch (Exception ex) {
			Log.log(ex);
			ex.printStackTrace();
		}
	}

	public About(JFrame own) {
		super(own, "关于我");
		panel1 = new JPanel();
		jLabel1 = new JLabel();
		layOut = new GridBagLayout();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();
		jLabel4 = new JLabel();
		jLabel5 = new JLabel();
		jLabel6 = new JLabel();
		jLabel7 = new JLabel();
		jEditorPane = new JEditorPane();
		setDefaultCloseOperation(2);
		try {
			jbInit();
		} catch (Exception ex) {
			Log.log(ex);
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		panel1.setLayout(layOut);
		Font font = new Font("Dialog", 0, 14);
		jLabel1.setText("代码生成器" + AppConstants.APP_VERSION + " 作者:五期提高班 李坤");
		jLabel2.setText(AppConstants.APP_COPYRIGHT);
		jLabel3.setText(AppConstants.APP_AUTHOR_CONTECT);
		jLabel1.setFont(font);
		jLabel1.setHorizontalAlignment(0);
		jLabel1.setHorizontalTextPosition(0);
		jLabel2.setFont(font);
		jLabel2.setHorizontalAlignment(0);
		jLabel2.setHorizontalTextPosition(0);
		jLabel3.setFont(font);
		jLabel3.setHorizontalAlignment(0);
		jLabel3.setHorizontalTextPosition(0);
		jLabel4.setPreferredSize(new Dimension(6, 16));
		jLabel4.setText("    ");
		jLabel5.setText("    ");
		jLabel6.setText("    ");
		jLabel7.setText("    ");
		jEditorPane.setFont(new Font("Dialog", 0, 11));
		getContentPane().add(panel1);
		panel1.add(jLabel2, new GridBagConstraints(1, 2, 2, 1, 0.0D, 0.0D, 10,
				2, new Insets(0, 0, 0, 0), 56, 0));
		panel1.add(jLabel3, new GridBagConstraints(1, 3, 2, 1, 0.0D, 0.0D, 10,
				2, new Insets(0, 0, 0, 0), 0, 0));
		panel1.add(jLabel1, new GridBagConstraints(1, 1, 2, 1, 0.0D, 0.0D, 10,
				2, new Insets(0, 0, 0, 0), 0, 0));
		panel1.add(jLabel4, new GridBagConstraints(2, 4, 1, 1, 0.0D, 0.0D, 10,
				0, new Insets(0, 0, 0, 0), 22, 0));
		panel1.add(jLabel5, new GridBagConstraints(1, 0, 2, 1, 0.0D, 0.0D, 10,
				0, new Insets(0, 0, 0, 0), 0, 0));
		panel1.add(jLabel6, new GridBagConstraints(3, 2, 1, 1, 0.0D, 0.0D, 10,
				0, new Insets(0, 0, 0, 0), 0, 0));
		panel1.add(jLabel7, new GridBagConstraints(0, 1, 1, 2, 0.0D, 0.0D, 10,
				0, new Insets(0, 0, 0, 0), 0, 0));
	}

	JPanel panel1;
	JLabel jLabel1;
	GridBagLayout layOut;
	JLabel jLabel2;
	JLabel jLabel3;
	JLabel jLabel4;
	JLabel jLabel5;
	JLabel jLabel6;
	JLabel jLabel7;
	JEditorPane jEditorPane;
}