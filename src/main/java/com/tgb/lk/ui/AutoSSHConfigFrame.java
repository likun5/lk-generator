package com.tgb.lk.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;

import com.tgb.lk.config.Config;
import com.tgb.lk.config.ConfigXmlUtil;
import com.tgb.lk.table.Log;
import com.tgb.lk.util.FileUtil;

public class AutoSSHConfigFrame extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	public AutoSSHConfigFrame(JFrame own) {
		super(own, "\u914D\u7F6E\u4FE1\u606F");
		getContentPane().setFont(new Font("宋体", Font.PLAIN, 12));
		layOut = new GridBagLayout();
		layOut.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		layOut.columnWeights = new double[] { 0.0, 1.0, 1.0, 0.0, 0.0 };
		beansDir = new JTextField();
		destCodeDir = new JTextField();
		buttonGroup1 = new ButtonGroup();
		setDefaultCloseOperation(2);
		try {
			jbInit();
		} catch (Exception ex) {
			Log.log(ex);
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		Font font = new Font("Dialog", 0, 14);
		getContentPane().setLayout(layOut);
		beansDir.setText("");
		destCodeDir.setText("");
		setFont(font);
		setForeground(Color.black);
		setModal(true);
		setResizable(false);

		label = new JLabel("");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 0;
		getContentPane().add(label, gbc_label);

		lblNewLabel_3 = new JLabel("");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 1;
		gbc_lblNewLabel_3.gridy = 1;
		getContentPane().add(lblNewLabel_3, gbc_lblNewLabel_3);

		lblothers = new JLabel("模板文件根路径:");
		lblothers.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_lblothers = new GridBagConstraints();
		gbc_lblothers.anchor = GridBagConstraints.EAST;
		gbc_lblothers.insets = new Insets(0, 0, 5, 5);
		gbc_lblothers.gridx = 1;
		gbc_lblothers.gridy = 2;
		getContentPane().add(lblothers, gbc_lblothers);

		templatesDir = new JTextField();
		GridBagConstraints gbc_templatesDir = new GridBagConstraints();
		gbc_templatesDir.insets = new Insets(0, 0, 5, 5);
		gbc_templatesDir.fill = GridBagConstraints.HORIZONTAL;
		gbc_templatesDir.gridx = 2;
		gbc_templatesDir.gridy = 2;
		getContentPane().add(templatesDir, gbc_templatesDir);
		templatesDir.setColumns(10);

		jButton4 = new JButton();
		jButton4.setText("浏  览");
		jButton4.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_jButton4 = new GridBagConstraints();
		gbc_jButton4.ipady = -4;
		gbc_jButton4.insets = new Insets(0, 0, 5, 5);
		gbc_jButton4.gridx = 3;
		gbc_jButton4.gridy = 2;
		jButton4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton4_actionPerformed(e);
			}
		});
		getContentPane().add(jButton4, gbc_jButton4);

		label_1 = new JLabel("非模板文件根路径:");
		label_1.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 3;
		getContentPane().add(label_1, gbc_label_1);

		fixedFilesDir = new JTextField();
		GridBagConstraints gbc_fixedFilesDir = new GridBagConstraints();
		gbc_fixedFilesDir.insets = new Insets(0, 0, 5, 5);
		gbc_fixedFilesDir.fill = GridBagConstraints.HORIZONTAL;
		gbc_fixedFilesDir.gridx = 2;
		gbc_fixedFilesDir.gridy = 3;
		getContentPane().add(fixedFilesDir, gbc_fixedFilesDir);
		fixedFilesDir.setColumns(30);

		jButton5 = new JButton();
		jButton5.setText("浏  览");
		jButton5.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_jButton5 = new GridBagConstraints();
		gbc_jButton5.ipady = -4;
		gbc_jButton5.insets = new Insets(0, 0, 5, 5);
		gbc_jButton5.gridx = 3;
		gbc_jButton5.gridy = 3;
		jButton5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton5_actionPerformed(e);
			}
		});
		getContentPane().add(jButton5, gbc_jButton5);

		lbljar = new JLabel("引用第三方jar包路径:");
		lbljar.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_lbljar = new GridBagConstraints();
		gbc_lbljar.anchor = GridBagConstraints.EAST;
		gbc_lbljar.insets = new Insets(0, 0, 5, 5);
		gbc_lbljar.gridx = 1;
		gbc_lbljar.gridy = 4;
		getContentPane().add(lbljar, gbc_lbljar);

		libsDir = new JTextField();
		GridBagConstraints gbc_libsDir = new GridBagConstraints();
		gbc_libsDir.insets = new Insets(0, 0, 5, 5);
		gbc_libsDir.fill = GridBagConstraints.HORIZONTAL;
		gbc_libsDir.gridx = 2;
		gbc_libsDir.gridy = 4;
		getContentPane().add(libsDir, gbc_libsDir);
		libsDir.setColumns(10);

		jButton6 = new JButton();
		jButton6.setText("浏  览");
		jButton6.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_jButton6 = new GridBagConstraints();
		gbc_jButton6.ipady = -4;
		gbc_jButton6.insets = new Insets(0, 0, 5, 5);
		gbc_jButton6.gridx = 3;
		gbc_jButton6.gridy = 4;
		jButton6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton6_actionPerformed(e);
			}
		});
		getContentPane().add(jButton6, gbc_jButton6);

		lblbean = new JLabel("元数据路径:");
		lblbean.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_lblbean = new GridBagConstraints();
		gbc_lblbean.insets = new Insets(0, 0, 5, 5);
		gbc_lblbean.anchor = GridBagConstraints.EAST;
		gbc_lblbean.gridx = 1;
		gbc_lblbean.gridy = 5;
		getContentPane().add(lblbean, gbc_lblbean);

		jButton7 = new JButton();
		jButton7.setText("浏  览");
		jButton7.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_jButton7 = new GridBagConstraints();
		gbc_jButton7.ipady = -4;
		gbc_jButton7.insets = new Insets(0, 0, 5, 5);
		gbc_jButton7.gridx = 3;
		gbc_jButton7.gridy = 5;
		jButton7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton7_actionPerformed(e);
			}
		});
		getContentPane().add(jButton7, gbc_jButton7);

		lblNewLabel = new JLabel("生成的代码根路径:");
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 8;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		getContentPane().add(
				destCodeDir,
				new GridBagConstraints(2, 8, 1, 1, 0.0D, 0.0D, 17, 2,
						new Insets(0, 0, 5, 5), 0, 0));
		getContentPane().add(
				beansDir,
				new GridBagConstraints(2, 5, 1, 1, 0.0D, 0.0D, 17, 2,
						new Insets(0, 0, 5, 5), 0, 0));

		jButton8 = new JButton();
		jButton8.setText("浏  览");
		jButton8.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_jButton8 = new GridBagConstraints();
		gbc_jButton8.ipady = -4;
		gbc_jButton8.insets = new Insets(0, 0, 5, 5);
		gbc_jButton8.gridx = 3;
		gbc_jButton8.gridy = 8;
		jButton8.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton8_actionPerformed(e);
			}
		});
		getContentPane().add(jButton8, gbc_jButton8);

		lblNewLabel_2 = new JLabel("注入模板的键值配置:");
		lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 9;
		getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);

		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 6;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 9;
		getContentPane().add(scrollPane, gbc_scrollPane);

		ctxConfig = new JTextArea();
		scrollPane.setViewportView(ctxConfig);
		ctxConfig.setWrapStyleWord(true);
		ctxConfig.setToolTipText("\r\n");
		ctxConfig.setRows(5);
		ctxConfig.setLineWrap(true);

		lblNewLabel_7 = new JLabel("一个键值对以等号(=)分隔");
		lblNewLabel_7.setFont(new Font("黑体", Font.PLAIN, 11));
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 1;
		gbc_lblNewLabel_7.gridy = 10;
		getContentPane().add(lblNewLabel_7, gbc_lblNewLabel_7);

		lblNewLabel_8 = new JLabel("不同键值对以换行进行分隔");
		lblNewLabel_8.setFont(new Font("黑体", Font.PLAIN, 11));
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 1;
		gbc_lblNewLabel_8.gridy = 11;
		getContentPane().add(lblNewLabel_8, gbc_lblNewLabel_8);

		lblNewLabel_9 = new JLabel("例:配置author=李坤,\n模板可引用${author}");
		lblNewLabel_9.setFont(new Font("黑体", Font.PLAIN, 11));
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 1;
		gbc_lblNewLabel_9.gridy = 12;
		getContentPane().add(lblNewLabel_9, gbc_lblNewLabel_9);

		lblNewLabel_6 = new JLabel("");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_6.gridx = 4;
		gbc_lblNewLabel_6.gridy = 13;
		getContentPane().add(lblNewLabel_6, gbc_lblNewLabel_6);

		lblNewLabel_4 = new JLabel("");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 14;
		getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);
		beansDir.setColumns(10);
		destCodeDir.setColumns(10);
		jButton2 = new JButton();
		jButton2.setFont(font);
		jButton2.setText("\u91CD  \u7F6E");
		jButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton2_actionPerformed(e);
			}
		});
		jButton1 = new JButton();
		jButton1.setFont(font);
		jButton1.setText("确  定");
		jButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jButton1_actionPerformed(e);
			}
		});

		chkDbSource = new JCheckBox(
				"读取数据库数据用于生成代码(注:1.需保证数据源配置正确 2.数据表中数据较多时可能导致生成代码速度变慢,请慎用!)");
		chkDbSource.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_chkDbSource = new GridBagConstraints();
		gbc_chkDbSource.gridwidth = 3;
		gbc_chkDbSource.anchor = GridBagConstraints.WEST;
		gbc_chkDbSource.insets = new Insets(0, 0, 5, 5);
		gbc_chkDbSource.gridx = 1;
		gbc_chkDbSource.gridy = 15;
		getContentPane().add(chkDbSource, gbc_chkDbSource);
		getContentPane().add(
				jButton1,
				new GridBagConstraints(1, 16, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 5, 5), 0, 0));
		getContentPane().add(
				jButton2,
				new GridBagConstraints(2, 16, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 5, 5), 0, 0));
		cancel = new JButton();
		cancel.setFont(font);
		cancel.setText("\u53D6  \u6D88");
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancel_actionPerformed(e);
			}
		});
		getContentPane().add(
				cancel,
				new GridBagConstraints(3, 16, 1, 1, 0.0D, 0.0D,
						GridBagConstraints.CENTER, GridBagConstraints.NONE,
						new Insets(0, 0, 5, 5), 0, 0));

		lblNewLabel_5 = new JLabel("");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_5.gridx = 1;
		gbc_lblNewLabel_5.gridy = 17;
		getContentPane().add(lblNewLabel_5, gbc_lblNewLabel_5);
		initParam();
	}

	private void initParam() {
		ConfigXmlUtil util = new ConfigXmlUtil();
		Config config = util.getConfig();
		templatesDir.setText(config.getTemplatesDir());
		fixedFilesDir.setText(config.getFixedFilesDir());
		libsDir.setText(config.getLibsDir());
		beansDir.setText(config.getBeansDir());
		destCodeDir.setText(config.getDestCodeDir());
		ctxConfig.setText(config.getCtxConfig());

	}

	@Override
	public void actionPerformed(ActionEvent actionevent) {
	}

	void jButton1_actionPerformed(ActionEvent e) {
		ConfigXmlUtil util = new ConfigXmlUtil();
		Config config = util.getConfig();
		config.setTemplatesDir(templatesDir.getText().trim());
		config.setFixedFilesDir(fixedFilesDir.getText().trim());
		config.setLibsDir(libsDir.getText().trim());
		config.setBeansDir(beansDir.getText().trim());
		config.setDestCodeDir(destCodeDir.getText().trim());
		config.setCtxConfig(ctxConfig.getText().trim());
		config.setChkDBSource(chkDbSource.isSelected());

		util.saveConfig(config);
		Log.log("保存配置文件成功", 3);
		FileUtil.newFolder(config.getTemplatesDir());
		FileUtil.newFolder(config.getFixedFilesDir());
		FileUtil.newFolder(config.getBeansDir());
		FileUtil.newFolder(config.getLibsDir());
		dispose();
		setVisible(false);
	}

	void jButton2_actionPerformed(ActionEvent e) {
		initParam();
	}

	void jButton3_actionPerformed(ActionEvent e) {
		// JFileChooser jFileChooser1 = new JFileChooser();
		// jFileChooser1.setFont(new Font("Dialog", 0, 12));
		// jFileChooser1.setFileSelectionMode(1);
		// jFileChooser1.setCurrentDirectory(new File(path.getText()));
		// int state = jFileChooser1.showOpenDialog(getContentPane());
		// if(state != 1)
		// {
		// File file = jFileChooser1.getSelectedFile();
		// if(file != null && file.isDirectory())
		// path.setText(file.getAbsolutePath());
		// }
	}

	void jButton4_actionPerformed(ActionEvent e) {
		JFileChooser jFileChooser1 = new JFileChooser();
		jFileChooser1.setFont(new Font("Dialog", 0, 12));
		jFileChooser1.setFileSelectionMode(1);
		jFileChooser1.setCurrentDirectory(new File(templatesDir.getText()));
		int state = jFileChooser1.showOpenDialog(getContentPane());
		if (state != 1) {
			File file = jFileChooser1.getSelectedFile();
			if (file != null && file.isDirectory()) {
				templatesDir.setText(file.getAbsolutePath());
			}
		}
	}

	void jButton5_actionPerformed(ActionEvent e) {
		JFileChooser jFileChooser1 = new JFileChooser();
		jFileChooser1.setFont(new Font("Dialog", 0, 12));
		jFileChooser1.setFileSelectionMode(1);
		jFileChooser1.setCurrentDirectory(new File(fixedFilesDir.getText()));
		int state = jFileChooser1.showOpenDialog(getContentPane());
		if (state != 1) {
			File file = jFileChooser1.getSelectedFile();
			if (file != null && file.isDirectory()) {
				fixedFilesDir.setText(file.getAbsolutePath());
			}
		}
	}

	void jButton6_actionPerformed(ActionEvent e) {
		JFileChooser jFileChooser1 = new JFileChooser();
		jFileChooser1.setFont(new Font("Dialog", 0, 12));
		jFileChooser1.setFileSelectionMode(1);
		jFileChooser1.setCurrentDirectory(new File(libsDir.getText()));
		int state = jFileChooser1.showOpenDialog(getContentPane());
		if (state != 1) {
			File file = jFileChooser1.getSelectedFile();
			if (file != null && file.isDirectory()) {
				libsDir.setText(file.getAbsolutePath());
			}
		}
	}

	void jButton7_actionPerformed(ActionEvent e) {
		JFileChooser jFileChooser1 = new JFileChooser();
		jFileChooser1.setFont(new Font("Dialog", 0, 12));
		jFileChooser1.setFileSelectionMode(1);
		jFileChooser1.setCurrentDirectory(new File(beansDir.getText()));
		int state = jFileChooser1.showOpenDialog(getContentPane());
		if (state != 1) {
			File file = jFileChooser1.getSelectedFile();
			if (file != null && file.isDirectory()) {
				beansDir.setText(file.getAbsolutePath());
			}
		}
	}

	void jButton8_actionPerformed(ActionEvent e) {
		JFileChooser jFileChooser1 = new JFileChooser();
		jFileChooser1.setFont(new Font("Dialog", 0, 12));
		jFileChooser1.setFileSelectionMode(1);
		jFileChooser1.setCurrentDirectory(new File(destCodeDir.getText()));
		int state = jFileChooser1.showOpenDialog(getContentPane());
		if (state != 1) {
			File file = jFileChooser1.getSelectedFile();
			if (file != null && file.isDirectory()) {
				destCodeDir.setText(file.getAbsolutePath());
			}
		}
	}

	void cancel_actionPerformed(ActionEvent e) {
		dispose();
		setVisible(false);
	}

	GridBagLayout layOut;
	JTextField beansDir;
	JTextField destCodeDir;
	JButton jButton1;
	JButton jButton2;
	JButton cancel;
	ButtonGroup buttonGroup1;
	private JTextField fixedFilesDir;
	private JLabel label_1;
	private JLabel lblbean;
	private JLabel lblNewLabel;
	private JLabel lblothers;
	private JTextField templatesDir;
	private JLabel lbljar;
	private JTextField libsDir;
	private JLabel label;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JLabel lblNewLabel_6;
	private JTextArea ctxConfig;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_7;
	private JLabel lblNewLabel_8;
	private JLabel lblNewLabel_9;
	private JButton jButton4;
	private JButton jButton5;
	private JButton jButton6;
	private JButton jButton7;
	private JButton jButton8;
	private JCheckBox chkDbSource;
	private JScrollPane scrollPane;
}