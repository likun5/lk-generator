package com.tgb.lk.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;

import javax.swing.*;

import com.tgb.lk.table.DBManager;
import com.tgb.lk.table.Log;
import com.tgb.lk.table.TableConfig;
import com.tgb.lk.util.AppConstants;
import com.tgb.lk.util.FileUtil;

public class ConfigFrame extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	public ConfigFrame(JFrame own) {
		super(own, "\u914D\u7F6E\u4FE1\u606F");
		layOut = new GridBagLayout();
		layOut.rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		layOut.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0 };
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		jLabel4 = new JLabel();
		jLabel5 = new JLabel();
		jLabel7 = new JLabel();
		// packagePath = new JTextField();
		// packagePath.setFont(new Font("宋体", Font.PLAIN, 12));
		path = new JTextField();
		path.setFont(new Font("SimSun", Font.PLAIN, 12));
		jLabel8 = new JLabel();
		buttonGroup1 = new ButtonGroup();
		setDefaultCloseOperation(2);
		try {
			jbInit();
		} catch (Exception ex) {
			Log.log(ex);
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		Font font = new Font("Dialog", 0, 14);
		jLabel1.setFont(new Font("宋体", Font.PLAIN, 14));
		jLabel1.setText("\u6570\u636E\u5E93\u8FDE\u63A5\u4FE1\u606F\uFF1A");
		getContentPane().setLayout(layOut);
		jLabel2.setFont(new Font("宋体", Font.PLAIN, 14));
		jLabel2.setText("数据库连接URL示例:");
		jLabel4.setFont(new Font("宋体", Font.PLAIN, 14));
		jLabel4.setText("密码:");
		jLabel5.setFont(new Font("宋体", Font.PLAIN, 14));
		jLabel5.setText("\u4EE3\u7801\u76F8\u5173\u4FE1\u606F:");
		jLabel7.setFont(new Font("宋体", Font.PLAIN, 14));
		jLabel7.setForeground(Color.black);
		jLabel7.setText("元数据存放路径：");
		path.setText("");
		setFont(font);
		setForeground(Color.black);
		setModal(true);
		setResizable(false);
		jLabel8.setText(" ");
		getContentPane().add(
				jLabel1,
				new GridBagConstraints(0, 0, 2, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 5, 5), 0, 0));
		getContentPane().add(
				jLabel2,
				new GridBagConstraints(0, 1, 2, 1, 0.0D, 0.0D, 13, 0,
						new Insets(0, 0, 5, 5), 0, 6));

		txtpnJdbcmysqlmydb = new JTextPane();
		txtpnJdbcmysqlmydb.setFont(new Font("宋体", Font.PLAIN, 14));
		txtpnJdbcmysqlmydb.setEditable(false);
		txtpnJdbcmysqlmydb.setText("jdbc:mysql://127.0.0.1:3306/mydb");
		GridBagConstraints gbc_txtpnJdbcmysqlmydb = new GridBagConstraints();
		gbc_txtpnJdbcmysqlmydb.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnJdbcmysqlmydb.fill = GridBagConstraints.BOTH;
		gbc_txtpnJdbcmysqlmydb.gridx = 2;
		gbc_txtpnJdbcmysqlmydb.gridy = 1;
		getContentPane().add(txtpnJdbcmysqlmydb, gbc_txtpnJdbcmysqlmydb);

		txtpnJdbcoraclethinmydb = new JTextPane();
		txtpnJdbcoraclethinmydb.setFont(new Font("宋体", Font.PLAIN, 14));
		txtpnJdbcoraclethinmydb.setEditable(false);
		txtpnJdbcoraclethinmydb
				.setText("jdbc:oracle:thin:@127.0.0.1:1521:mydb");
		GridBagConstraints gbc_txtpnJdbcoraclethinmydb = new GridBagConstraints();
		gbc_txtpnJdbcoraclethinmydb.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnJdbcoraclethinmydb.fill = GridBagConstraints.BOTH;
		gbc_txtpnJdbcoraclethinmydb.gridx = 2;
		gbc_txtpnJdbcoraclethinmydb.gridy = 2;
		getContentPane().add(txtpnJdbcoraclethinmydb,
				gbc_txtpnJdbcoraclethinmydb);
		jRadioOracle = new JRadioButton();
		jRadioOracle.setFont(new Font("宋体", Font.BOLD, 14));
		jRadioOracle.setText("Oracle\u6570\u636E\u5E93");

		txtpnJdbcjtdssqlservermydb = new JTextPane();
		txtpnJdbcjtdssqlservermydb.setFont(new Font("宋体", Font.PLAIN, 14));
		txtpnJdbcjtdssqlservermydb.setEditable(false);
		txtpnJdbcjtdssqlservermydb
				.setText("jdbc:jtds:sqlserver://127.0.0.1:1433/mydb");
		GridBagConstraints gbc_txtpnJdbcjtdssqlservermydb = new GridBagConstraints();
		gbc_txtpnJdbcjtdssqlservermydb.insets = new Insets(0, 0, 5, 5);
		gbc_txtpnJdbcjtdssqlservermydb.fill = GridBagConstraints.BOTH;
		gbc_txtpnJdbcjtdssqlservermydb.gridx = 2;
		gbc_txtpnJdbcjtdssqlservermydb.gridy = 3;
		getContentPane().add(txtpnJdbcjtdssqlservermydb,
				gbc_txtpnJdbcjtdssqlservermydb);

		jRadioMysql = new JRadioButton("MySQL数据库");
		buttonGroup1.add(jRadioMysql);
		GridBagConstraints gbc_jRadioMysql = new GridBagConstraints();
		gbc_jRadioMysql.insets = new Insets(0, 0, 5, 5);
		gbc_jRadioMysql.gridx = 1;
		gbc_jRadioMysql.gridy = 4;
		jRadioMysql.setFont(new Font("宋体", Font.BOLD, 14));
		getContentPane().add(jRadioMysql, gbc_jRadioMysql);
		getContentPane().add(
				jRadioOracle,
				new GridBagConstraints(2, 4, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 5, 5), 0, 0));
		buttonGroup1.add(jRadioOracle);
		jRadioSql = new JRadioButton();
		jRadioSql.setFont(new Font("宋体", Font.BOLD, 14));
		jRadioSql.setToolTipText("");
		jRadioSql.setText("SqlServer\u6570\u636E\u5E93");
		getContentPane().add(
				jRadioSql,
				new GridBagConstraints(3, 4, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 5, 0), 0, 0));
		buttonGroup1.add(jRadioSql);

		lblurl = new JLabel("数据库连接URL:");
		lblurl.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_lblurl = new GridBagConstraints();
		gbc_lblurl.insets = new Insets(0, 0, 5, 5);
		gbc_lblurl.anchor = GridBagConstraints.EAST;
		gbc_lblurl.gridx = 1;
		gbc_lblurl.gridy = 5;
		getContentPane().add(lblurl, gbc_lblurl);
		url = new JTextField();
		url.setFont(new Font("宋体", Font.PLAIN, 14));
		url.setColumns(20);
		getContentPane().add(
				url,
				new GridBagConstraints(2, 5, 2, 1, 0.0D, 0.0D, 17, 2,
						new Insets(0, 0, 5, 0), 0, 0));

		label = new JLabel("用户名:");
		label.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.anchor = GridBagConstraints.EAST;
		gbc_label.gridx = 1;
		gbc_label.gridy = 6;
		getContentPane().add(label, gbc_label);
		userName = new JTextField();
		userName.setFont(new Font("宋体", Font.PLAIN, 12));
		userName.setText("");
		getContentPane().add(
				userName,
				new GridBagConstraints(2, 6, 1, 1, 0.0D, 0.0D, 17, 2,
						new Insets(0, 0, 5, 5), 0, 0));
		userName.setColumns(10);
		getContentPane().add(
				jLabel4,
				new GridBagConstraints(0, 7, 2, 1, 0.0D, 0.0D, 13, 0,
						new Insets(0, 0, 5, 5), 0, 11));
		passWd = new JPasswordField();
		passWd.setFont(new Font("宋体", Font.PLAIN, 12));
		passWd.setText("");
		getContentPane().add(
				passWd,
				new GridBagConstraints(2, 7, 1, 1, 0.0D, 0.0D, 17, 2,
						new Insets(0, 0, 5, 5), 0, 0));
		passWd.setColumns(10);

		btnTestConn = new JButton("测试连接");
		btnTestConn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int data_type = 0;
				if (jRadioSql.isSelected()) {
					data_type = AppConstants.DB_SQLSERVER;
				} else if (jRadioOracle.isSelected()) {
					data_type = AppConstants.DB_ORACLE;
				} else if (jRadioMysql.isSelected()) {
					data_type = AppConstants.DB_MYSQL;
				}
				Connection conn = DBManager.getInstance().getConnection(
						data_type, url.getText(), userName.getText(),
						String.valueOf(passWd.getPassword()));
				if (conn != null) {
					Log.log("测试连接-->连接成功!", 4);
					lblTestConn.setText("测试连接-->连接成功!");
				} else {
					Log.log("测试连接-->连接失败!", 4);
					lblTestConn.setText("测试连接-->连接失败!");
				}
			}
		});
		btnTestConn.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_btnTestConn = new GridBagConstraints();
		gbc_btnTestConn.ipady = -4;
		gbc_btnTestConn.anchor = GridBagConstraints.WEST;
		gbc_btnTestConn.insets = new Insets(0, 0, 5, 5);
		gbc_btnTestConn.gridx = 3;
		gbc_btnTestConn.gridy = 7;
		getContentPane().add(btnTestConn, gbc_btnTestConn);
		getContentPane().add(
				jLabel5,
				new GridBagConstraints(0, 9, 2, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 5, 5), 0, 11));

		lblTestConn = new JLabel("");
		lblTestConn.setFont(new Font("宋体", Font.BOLD, 12));
		GridBagConstraints gbc_lblTestConn = new GridBagConstraints();
		gbc_lblTestConn.anchor = GridBagConstraints.NORTH;
		gbc_lblTestConn.insets = new Insets(0, 0, 5, 5);
		gbc_lblTestConn.gridx = 2;
		gbc_lblTestConn.gridy = 9;
		getContentPane().add(lblTestConn, gbc_lblTestConn);
		getContentPane().add(
				jLabel7,
				new GridBagConstraints(0, 11, 2, 1, 0.0D, 0.0D, 13, 0,
						new Insets(0, 0, 5, 5), 0, 4));
		// getContentPane().add(
		// packagePath,
		// new GridBagConstraints(2, 9, 1, 1, 0.0D, 0.0D, 17, 2,
		// new Insets(0, 0, 5, 5), 0, 0));
		getContentPane().add(
				path,
				new GridBagConstraints(2, 11, 1, 1, 0.0D, 0.0D, 17, 2,
						new Insets(0, 0, 5, 5), 0, 0));

		btnView = new JButton(" 浏  览 ");
		btnView.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectFile();
			}
		});
		btnView.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_btnView = new GridBagConstraints();
		gbc_btnView.ipady = -4;
		gbc_btnView.anchor = GridBagConstraints.WEST;
		gbc_btnView.insets = new Insets(0, 0, 5, 5);
		gbc_btnView.gridx = 3;
		gbc_btnView.gridy = 11;
		getContentPane().add(btnView, gbc_btnView);

		label_1 = new JLabel("去掉表名前缀(可为空):");
		label_1.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 12;
		getContentPane().add(label_1, gbc_label_1);

		txtStartChar = new JTextField();
		txtStartChar.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_txtStartChar = new GridBagConstraints();
		gbc_txtStartChar.insets = new Insets(0, 0, 5, 5);
		gbc_txtStartChar.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtStartChar.gridx = 2;
		gbc_txtStartChar.gridy = 12;
		getContentPane().add(txtStartChar, gbc_txtStartChar);
		txtStartChar.setColumns(10);

		label_2 = new JLabel("去掉表名后缀(可为空):");
		label_2.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.EAST;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 13;
		getContentPane().add(label_2, gbc_label_2);

		txtEndChar = new JTextField();
		txtEndChar.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_txtEndChar = new GridBagConstraints();
		gbc_txtEndChar.insets = new Insets(0, 0, 5, 5);
		gbc_txtEndChar.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtEndChar.gridx = 2;
		gbc_txtEndChar.gridy = 13;
		getContentPane().add(txtEndChar, gbc_txtEndChar);
		txtEndChar.setColumns(10);

		label_3 = new JLabel("去掉表名中符号(可为空):");
		label_3.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.EAST;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 14;
		getContentPane().add(label_3, gbc_label_3);

		txtMidChar = new JTextField();
		GridBagConstraints gbc_txtMidChar = new GridBagConstraints();
		gbc_txtMidChar.insets = new Insets(0, 0, 5, 5);
		gbc_txtMidChar.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMidChar.gridx = 2;
		gbc_txtMidChar.gridy = 14;
		getContentPane().add(txtMidChar, gbc_txtMidChar);
		txtMidChar.setColumns(10);

		checkBean = new JCheckBox("大写表名下划线后第一个字母");
		checkBean.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_beanCheck = new GridBagConstraints();
		gbc_beanCheck.insets = new Insets(0, 0, 5, 5);
		gbc_beanCheck.gridx = 1;
		gbc_beanCheck.gridy = 15;
		getContentPane().add(checkBean, gbc_beanCheck);
		checkBean.addActionListener(this);

		checkField = new JCheckBox("去掉属性中下划线并大写其后第一个字母");
		checkField.setFont(new Font("宋体", Font.PLAIN, 14));
		GridBagConstraints gbc_fieldCheck = new GridBagConstraints();
		gbc_fieldCheck.insets = new Insets(0, 0, 5, 5);
		gbc_fieldCheck.gridx = 2;
		gbc_fieldCheck.gridy = 15;
		getContentPane().add(checkField, gbc_fieldCheck);
		checkField.addActionListener(this);
		getContentPane().add(
				jLabel8,
				new GridBagConstraints(0, 16, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(0, 0, 0, 5), 0, 20));
		// packagePath.setColumns(10);
		path.setColumns(10);

		btnOk = new JButton("确 定");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveConfig();
			}
		});
		btnOk.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 0, 5);
		gbc_btnOk.gridx = 1;
		gbc_btnOk.gridy = 16;
		getContentPane().add(btnOk, gbc_btnOk);

		btnReset = new JButton("重 置");
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				initParam();
			}
		});
		btnReset.setFont(new Font("宋体", Font.PLAIN, 12));
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.insets = new Insets(0, 0, 0, 5);
		gbc_btnReset.gridx = 2;
		gbc_btnReset.gridy = 16;
		getContentPane().add(btnReset, gbc_btnReset);

		btnCancel = new JButton("取 消");
		btnCancel.setFont(new Font("宋体", Font.PLAIN, 12));
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				setVisible(false);
			}
		});
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.gridx = 3;
		gbc_btnCancel.gridy = 16;
		getContentPane().add(btnCancel, gbc_btnCancel);
		initParam();
	}

	private void initParam() {
		TableConfig config = TableConfig.getInstance();
		// packagePath.setText(config.getPackagePath());
		passWd.setText(config.getDbPasswd());
		userName.setText(config.getDbUsername());
		url.setText(config.getDbConnectString());
		path.setText(config.getPath());
		if (config.getDataBaseType() == AppConstants.DB_SQLSERVER) {
			jRadioSql.setSelected(true);
		} else if (config.getDataBaseType() == AppConstants.DB_ORACLE) {
			jRadioOracle.setSelected(true);
		} else {
			jRadioMysql.setSelected(true);
		}

		if ("true".equals(config.getCheckBean())) {
			checkBean.setSelected(true);
		} else {
			checkBean.setSelected(false);
		}

		if ("true".equals(config.getCheckField())) {
			checkField.setSelected(true);
		} else {
			checkField.setSelected(false);
		}
		txtStartChar.setText(config.getStartChar());
		txtEndChar.setText(config.getEndChar());
		txtMidChar.setText(config.getMidChar());
	}

	@Override
	public void actionPerformed(ActionEvent actionevent) {
	}

	private void saveConfig() {
		TableConfig config = TableConfig.getInstance();
		config.setDbConnectString(url.getText().trim());
		config.setDbPasswd(new String(passWd.getPassword()).trim());
		config.setDbUsername(userName.getText().trim());
		// config.setPackagePath(packagePath.getText().trim());
		config.setPath(path.getText().trim());
		// config.setSequence(sequece.getText());
		int data_type = 0;
		if (jRadioSql.isSelected()) {
			data_type = AppConstants.DB_SQLSERVER;
		} else if (jRadioOracle.isSelected()) {
			data_type = AppConstants.DB_ORACLE;
		} else if (jRadioMysql.isSelected()) {
			data_type = AppConstants.DB_MYSQL;
		}

		if (checkBean.isSelected()) {
			config.setCheckBean("true");
		} else {
			config.setCheckBean("false");
		}

		if (checkField.isSelected()) {
			config.setCheckField("true");
		} else {
			config.setCheckField("false");
		}

		config.setDataBaseType(data_type);
		config.setStartChar(txtStartChar.getText().trim());
		config.setEndChar(txtEndChar.getText().trim());
		config.setMidChar(txtMidChar.getText().trim());
		config.writeToFile(FileUtil.makeFilePath(TableConfig.proFilePath));
		Log.log("保存配置文件成功!", 3);
		dispose();
		setVisible(false);
	}

	private void selectFile() {
		JFileChooser jFileChooser1 = new JFileChooser();
		jFileChooser1.setFont(new Font("Dialog", 0, 12));
		jFileChooser1.setFileSelectionMode(1);
		jFileChooser1.setCurrentDirectory(new File(path.getText()));
		int state = jFileChooser1.showOpenDialog(getContentPane());
		if (state != 1) {
			File file = jFileChooser1.getSelectedFile();
			if (file != null && file.isDirectory()) {
				path.setText(file.getAbsolutePath());
			}
		}
	}

	GridBagLayout layOut;
	JLabel jLabel1;
	JTextField userName;
	JLabel jLabel2;
	JLabel jLabel4;
	JTextField url;
	JPasswordField passWd;
	JLabel jLabel5;
	JLabel jLabel7;
	// JTextField packagePath;
	JTextField path;
	JLabel jLabel8;
	JRadioButton jRadioOracle;
	JRadioButton jRadioSql;
	ButtonGroup buttonGroup1;
	private JRadioButton jRadioMysql;
	private JLabel label;
	private JLabel lblurl;
	private JLabel label_1;
	private JTextField txtStartChar;
	private JLabel label_2;
	private JTextField txtEndChar;
	private JLabel label_3;
	private JTextField txtMidChar;
	private JTextPane txtpnJdbcjtdssqlservermydb;
	private JTextPane txtpnJdbcoraclethinmydb;
	private JTextPane txtpnJdbcmysqlmydb;
	private JCheckBox checkBean;
	private JCheckBox checkField;
	private JButton btnTestConn;
	private JLabel lblTestConn;
	private JButton btnOk;
	private JButton btnReset;
	private JButton btnCancel;
	private JButton btnView;
}