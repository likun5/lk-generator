package com.tgb.lk.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import com.tgb.lk.table.Log;
import com.tgb.lk.table.MysqlTableManager;
import com.tgb.lk.table.TableConfig;
import com.tgb.lk.table.OracleTableManager;
import com.tgb.lk.table.SQLTableManager;
import com.tgb.lk.table.TableManager;
import com.tgb.lk.util.AppConstants;

public class ConfigTable extends JDialog {
	private static final long serialVersionUID = 1L;
	private int db_type;

	public int getDb_type() {
		return db_type;
	}

	public void setDb_type(int dbType) {
		db_type = dbType;
	}

	public ConfigTable(Frame frame) {
		super(
				frame,
				"选择数据库表，以生成对应的元数据代码");
		layOut = new GridBagLayout();
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();
		jList1 = new JList(new DefaultListModel());
		jScrollPane1 = new JScrollPane(jList1);
		jList2 = new JList(new DefaultListModel());
		jScrollPane2 = new JScrollPane(jList2);
		jbadd = new JButton();
		jbmin = new JButton();
		jBok = new JButton();
		jBcan = new JButton();
		jBreset = new JButton();
		setDefaultCloseOperation(2);
		try {
			jbInit();
			init();
		} catch (Exception ex) {
			Log.log(ex);
			ex.printStackTrace();
		}
	}

	public ConfigTable() {
		this(null);
	}

	private void jbInit() throws Exception {
		Font font = new Font("Dialog", 0, 14);
		Insets inserts = new Insets(0, 0, 0, 0);
		jLabel1.setFont(new Font("Dialog", 1, 16));
		jLabel2.setFont(font);
		jLabel3.setFont(font);
		jList1.setFont(font);
		jScrollPane1.setFont(font);
		jList2.setFont(font);
		jScrollPane2.setFont(font);
		jbadd.setFont(font);
		jbmin.setFont(font);
		jBok.setFont(font);
		jBcan.setFont(font);
		jBreset.setFont(font);
		jBreset.setToolTipText("");
		jBreset.setActionCommand("\u5237 \u65B0");
		jLabel1
				.setText("请选择需要操作的表");
		getContentPane().setLayout(layOut);
		jLabel2.setText("\u6570\u636E\u5E93\u8868\uFF1A");
		jLabel3.setText("\u5DF2\u9009\u62E9\u7684\u8868:");
		jList1.setFixedCellWidth(100);
		jList2.setFixedCellWidth(100);
		jbadd.setText("==>");
		jbadd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jbadd_actionPerformed(e);
			}
		});
		jbmin.setText("<==");
		jbmin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jbmin_actionPerformed(e);
			}
		});
		jBok.setText("\u786E \u5B9A");
		jBok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jBok_actionPerformed(e);
			}
		});
		jBcan.setText(" \u53D6 \u6D88");
		jBcan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jBcan_actionPerformed(e);
			}
		});
		jBreset.setText("\u5237 \u65B0");
		jBreset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jBreset_actionPerformed(e);
			}
		});
		getContentPane().add(
				jLabel1,
				new GridBagConstraints(0, 0, 3, 1, 0.0D, 0.0D, 10, 0, inserts,
						0, 0));
		getContentPane().add(
				jLabel3,
				new GridBagConstraints(2, 1, 1, 1, 0.0D, 0.0D, 10, 0, inserts,
						0, 0));
		getContentPane().add(
				jScrollPane1,
				new GridBagConstraints(0, 2, 1, 2, 0.0D, 0.0D, 10, 1,
						new Insets(0, 0, 0, 0), 87, 142));
		getContentPane().add(
				jScrollPane2,
				new GridBagConstraints(2, 2, 1, 2, 0.0D, 0.0D, 10, 1,
						new Insets(0, 0, 0, 0), 87, 142));
		getContentPane().add(
				jLabel2,
				new GridBagConstraints(0, 1, 1, 1, 0.0D, 0.0D, 10, 0, inserts,
						0, 0));
		getContentPane().add(
				jbmin,
				new GridBagConstraints(1, 3, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(-1, 0, 1, 0), 0, 0));
		getContentPane().add(
				jbadd,
				new GridBagConstraints(1, 2, 1, 1, 0.0D, 0.0D, 10, 0,
						new Insets(49, 0, 0, 0), 0, 0));
		getContentPane().add(
				jBok,
				new GridBagConstraints(0, 4, 1, 1, 0.0D, 0.0D, 10, 0, inserts,
						0, 0));
		getContentPane().add(
				jBcan,
				new GridBagConstraints(2, 4, 1, 1, 0.0D, 0.0D, 10, 0, inserts,
						0, 0));
		getContentPane().add(
				jBreset,
				new GridBagConstraints(1, 4, 1, 1, 0.0D, 0.0D, 10, 0, inserts,
						0, 0));
		setModal(true);
	}

	@Override
	public void pack() {
		if (db_type != TableConfig.getInstance().getDataBaseType()) {
			init();
		}
		super.pack();
	}

	private void init() {
		db_type = TableConfig.getInstance().getDataBaseType();
		DefaultListModel model1 = (DefaultListModel) jList1.getModel();
		DefaultListModel model2 = (DefaultListModel) jList2.getModel();
		model1.clear();
		model2.clear();
		TableManager t = null;
		int data_type = TableConfig.getInstance().getDataBaseType();
		if (data_type == AppConstants.DB_SQLSERVER) {
            t = SQLTableManager.getInstance();
        } else if (data_type == AppConstants.DB_ORACLE) {
			t = OracleTableManager.getInstance();
		} else {
			t = MysqlTableManager.getInstance();
		}
		String tables[] = t.getAllTableName();
		String stables[] = TableConfig.getInstance().getTableList();
		for (int i = 0; tables != null && i < tables.length; i++) {
            model1.addElement(tables[i]);
        }

		for (int i = 0; stables != null && i < stables.length; i++) {
            model2.addElement(stables[i]);
        }

	}

	void jBcan_actionPerformed(ActionEvent e) {
		dispose();
		setVisible(false);
	}

	void jBreset_actionPerformed(ActionEvent e) {
		init();
	}

	void jBok_actionPerformed(ActionEvent e) {
		ListModel model = jList2.getModel();
		String values[] = new String[model.getSize()];
		for (int i = 0; i < values.length; i++) {
            values[i] = (String) model.getElementAt(i);
        }

		TableConfig.getInstance().setTableList(values);
		String strValue = "";
		for (String string : values) {
			strValue += string + ",";
		}
		if (strValue.length() > 1) {
			strValue = strValue.substring(0, strValue.length() - 1);
		}
		Log.log("您已选中的表:" + strValue, 3);
		dispose();
		setVisible(false);
	}

	void jbadd_actionPerformed(ActionEvent e) {
		DefaultListModel model1 = (DefaultListModel) jList1.getModel();
		DefaultListModel model2 = (DefaultListModel) jList2.getModel();
		int indexs[] = jList1.getSelectedIndices();
		for (int i = 0; indexs != null && i < indexs.length; i++) {
			model2.addElement(model1.get(indexs[i] - i));
			model1.remove(indexs[i] - i);
		}

	}

	void jbmin_actionPerformed(ActionEvent e) {
		DefaultListModel model1 = (DefaultListModel) jList1.getModel();
		DefaultListModel model2 = (DefaultListModel) jList2.getModel();
		int indexs[] = jList2.getSelectedIndices();
		for (int i = 0; indexs != null && i < indexs.length; i++) {
			model1.addElement(model2.get(indexs[i] - i));
			model2.remove(indexs[i] - i);
		}

	}

	GridBagLayout layOut;
	JLabel jLabel1;
	JLabel jLabel2;
	JLabel jLabel3;
	JList jList1;
	JScrollPane jScrollPane1;
	JList jList2;
	JScrollPane jScrollPane2;
	JButton jbadd;
	JButton jbmin;
	JButton jBok;
	JButton jBcan;
	JButton jBreset;
}