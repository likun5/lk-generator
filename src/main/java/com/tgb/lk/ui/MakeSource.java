package com.tgb.lk.ui;

import com.tgb.lk.table.CreateModel;
import com.tgb.lk.table.Log;
import com.tgb.lk.table.MysqlTableManager;
import com.tgb.lk.table.TableConfig;
import com.tgb.lk.table.OracleTableManager;
import com.tgb.lk.table.SQLTableManager;
import com.tgb.lk.table.Table;
import com.tgb.lk.table.TableManager;
import com.tgb.lk.util.AppConstants;

class MakeSource extends Thread {

	public MakeSource() {
		flag = 0;
	}

	@Override
    public void run() {
		String tables[] = TableConfig.getInstance().getTableList();
		Table table = null;
		String path = "";
		if (tables != null && tables.length > 0) {
			for (int i = 0; i < tables.length && flag == 0; i++) {
				TableManager t = null;
				int data_type = TableConfig.getInstance().getDataBaseType();
				if (data_type == AppConstants.DB_SQLSERVER) {
					t = SQLTableManager.getInstance();
				} else if (data_type == AppConstants.DB_ORACLE) {
					t = OracleTableManager.getInstance();
				} else {
					t = MysqlTableManager.getInstance();
				}
				// table = t.getTableInfo(tables[i].toLowerCase());
				table = t.getTableInfo(tables[i]);
				path = CreateModel.getInstance().createModel(
						TableConfig.getInstance().getPath(), table);

			}
			Log.log("生成实体对象成功,生成的代码位置:" + path, 3);
		} else {
			Log.log("您没有选择表,请选择表后再试一次.", 3);
		}
	}

	public void setFlag(int f) {
		flag = f;
	}

	int flag;
}