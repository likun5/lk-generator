package com.tgb.lk.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target( { java.lang.annotation.ElementType.TYPE })
public @interface AutoBean {
	/**
	 * 中文名称
	 */
	public abstract String alias() default "";

	/**
	 * 表名
	 * 
	 * @return
	 */
	public abstract String table() default "";

	/**
	 * 附加参数数组
	 * 
	 * @return
	 */
	public abstract String[] args() default {};

}
