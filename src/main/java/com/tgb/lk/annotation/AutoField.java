package com.tgb.lk.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.FIELD})
public @interface AutoField {
    public static final String TYPE_INTEGER = "Integer";
    public static final String TYPE_DOUBLE = "Double";
    public static final String TYPE_STRING = "String";
    public static final String TYPE_DATE = "Date";
    public static final String TYPE_BOOLEAN = "Boolean";
    public static final String TYPE_LONG = "Long";
    public static final String TYPE_FLOAT = "Float";
    public static final String TYPE_BYTE = "byte[]";
    public static final String TYPE_BIGINTEGER = "BigInteger";
    public static final String TYPE_BIGDECIMAL = "BigDecimal";

    /**
     * 中文名称
     */
    public abstract String alias() default "";

    /**
     * 是否必填项,与nullabale相反
     */
    public abstract boolean isRequired() default false;

    /**
     * 是否主键
     */
    public abstract boolean isKey() default false;

    /**
     * 类型,可根据类型生成代码,用于套用默认的int/date等日期验证.
     */
    public abstract String type() default "";

    /**
     * 组合框
     */
    public abstract String[] combo() default {};

    /**
     * 属性对应的表中字段
     */
    public abstract String column() default "";

    /**
     * 数据库type
     */
    public abstract String columnType() default "";

    /**
     * 附加参数数组
     */
    public abstract String[] args() default {};

    /**
     * 数据库中字段长度
     */
    public abstract int length() default 255;

}
