package com.tgb.lk.table;

/**
 * 统一名称的工具类(为了避免整个工程中的命名不一致的情况出现)
 * 
 * @author likun
 */
public class NameUtil {
	private NameUtil() {
	}

	/**
	 * 获得表名,默认首字母大写
	 * 
	 * @return
	 */
	public static String setTableName(String s) {
		return StringUtil.setFirstCharUpcase(s);
	}

	/**
	 * 获得Model包名,包名默认全部变成小写后+".model"
	 * 
	 * @return
	 */
	public static String setModelPackageName(String s) {
		return s.toLowerCase();
	}

	/**
	 * 获得util包名,包名默认全部变成小写后+".util"
	 * 
	 * @return
	 */
	public static String setUtilsPackageName(String s) {
		return s.toLowerCase() + ".util";
	}

	/**
	 * 获得Dao包名,包名默认全部变成小写后+".dao"
	 * 
	 * @return
	 */
	public static String setDaoPackageName(String s) {
		return s.toLowerCase() + ".dao";
	}

	/**
	 * 获得Mgr包名,包名默认全部变成小写后+".mgr"
	 * 
	 * @return
	 */
	public static String setMgrPackageName(String s) {
		return s.toLowerCase() + ".mgr";
	}

	/**
	 * 获得DaoImpl包名,包名默认全部变成小写后+".dao.impl"
	 * 
	 * @return
	 */
	public static String setDaoImplPackageName(String s) {
		return s.toLowerCase() + ".dao.impl";
	}

	/**
	 * 获得Mgr包名,包名默认全部变成小写后+".mgr.impl"
	 * 
	 * @return
	 */
	public static String setMgrImplPackageName(String s) {
		return s.toLowerCase() + ".mgr.impl";
	}

	public static String setActionPackageName(String s) {
		return s.toLowerCase() + ".web.action";
	}

	/**
	 * 设置私有字段名,默认首字母小写
	 * 
	 * @param s
	 * @return
	 */
	public static String setPrivateFieldName(String s) {
		return StringUtil.setFirstCharLowcase(s);
	}

	/**
	 * 获得get方法的名称,默认为get+首字母大写.
	 * 
	 * @param s
	 * @return
	 */
	public static String setGetFieldName(String s) {
		return "get" + StringUtil.setFirstCharUpcase(s);
	}

	/**
	 * 获得get方法的名称,默认为get+首字母大写.
	 * 
	 * @param s
	 * @return
	 */
	public static String setSetFieldName(String s) {
		return "set" + StringUtil.setFirstCharUpcase(s);
	}

	/**
	 * Dao的name,默认是首字母大写,尾部加上Dao.
	 * 
	 * @param s
	 * @return
	 */
	public static String setDaoName(String s) {
		return StringUtil.setFirstCharUpcase(s) + "Dao";
	}

	/**
	 * DaoImpl的name,默认是首字母大写,尾部加上Dao.
	 * 
	 * @param s
	 * @return
	 */
	public static String setDaoImplName(String s) {
		return StringUtil.setFirstCharUpcase(s) + "DaoImpl";
	}

	/**
	 * Mgr的name,默认是首字母大写,尾部加上Mgr.
	 * 
	 * @param s
	 * @return
	 */
	public static String setMgrName(String s) {
		return StringUtil.setFirstCharUpcase(s) + "Mgr";
	}

	/**
	 * MgrImpl的name,默认是首字母大写,尾部加上Mgr.
	 * 
	 * @param s
	 * @return
	 */
	public static String setMgrImplName(String s) {
		return StringUtil.setFirstCharUpcase(s) + "MgrImpl";
	}

	/**
	 * 设置add方法的名称,默认是add;
	 * 
	 * @param s
	 * @return
	 */
	public static String setAddMethodName(String s) {
		return "add";
	}

	/**
	 * 设置delete方法的名称,默认是delete;
	 * 
	 * @param s
	 * @return
	 */
	public static String setDeleteMethodName(String s) {
		return "delete";
	}

	/**
	 * 设置findById方法的名称,默认是findById.
	 * 
	 * @param s
	 * @return
	 */
	public static String setFindByIdMethodName(String s) {
		return "findById";
	}

	/**
	 * 设置update方法的名称,默认是update.
	 * 
	 * @param s
	 * @return
	 */
	public static String setUpdateMethodName(String s) {
		return "update";
	}

	/**
	 * 设置searchAll方法的名称,默认是searchAll.
	 * 
	 * @param s
	 * @return
	 */
	public static String setSearchAllMethodName(String s) {
		return "searchAll";
	}

	/**
	 * 设置searchByOneField方法的名称,默认是searchByOneField.
	 * 
	 * @param s
	 * @return
	 */
	public static String setSearchByOneField(String s) {
		return "searchByOneField";
	}

	/**
	 * 设置Model的名字,默认首字母大写
	 * 
	 * @param s
	 * @return
	 */
	public static String setModelName(String s) {
		String startChar = TableConfig.getInstance().getStartChar();
		String endChar = TableConfig.getInstance().getEndChar();
		String midChar = TableConfig.getInstance().getMidChar();
		String checkBean = TableConfig.getInstance().getCheckBean();
		if ("true".equals(checkBean)) {
			s = StringUtil.set_FirstCharUpcase(s);
		}
		if ((!"".equals(startChar))
				&& s.toUpperCase().startsWith(startChar.toUpperCase())) {
			s = s.substring(startChar.length(), s.length());
		}
		if ((!"".equals(endChar))
				&& s.toUpperCase().endsWith(endChar.toUpperCase())) {
			s = s.substring(0, s.length() - endChar.length());
		}
		if (!"".equals(midChar)) {
			s = s.replaceAll(midChar, "");
		}
		return StringUtil.setFirstCharUpcase(s);
	}

	/**
	 * 设置实例对象的名字,默认首字母小写
	 * 
	 * @param s
	 * @return
	 */
	public static String setObjName(String s) {
		return StringUtil.setFirstCharLowcase(s);
	}

	/**
	 * 设置Dao实例对象的名字,默认首字母小写,尾部加Dao
	 * 
	 * @param s
	 * @return
	 */
	public static String setDaoObjName(String s) {
		return StringUtil.setFirstCharLowcase(s) + "Dao";

	}

	/**
	 * 设置Mgr实例对象的名字,默认首字母小写,尾部加Mgr
	 * 
	 * @param s
	 * @return
	 */
	public static String setMgrObjName(String s) {
		return StringUtil.setFirstCharLowcase(s) + "Mgr";
	}

	/**
	 * 设置Action的名字,默认首字母大写,尾部加Action
	 * 
	 * @param s
	 * @return
	 */
	public static String setActionName(String s) {
		return StringUtil.setFirstCharUpcase(s) + "Action";
	}

	/**
	 * 设置配置文件中Action的id名称
	 * 
	 * @param s
	 * @return
	 */
	public static String setActionId4Spring(String s) {
		return StringUtil.setFirstCharLowcase(s) + "Action";
	}

	/**
	 * 设置配置文件中Mgr的id名称
	 * 
	 * @param s
	 * @return
	 */
	public static String setMgrId4Spring(String s) {
		return StringUtil.setFirstCharLowcase(s) + "Mgr";
	}

	/**
	 * 设置配置文件中Dao的id名称
	 * 
	 * @param s
	 * @return
	 */
	public static String setDaoId4Spring(String s) {
		return StringUtil.setFirstCharLowcase(s) + "Dao";
	}

	/**
	 * 设置struts配置文件中Action的id名称
	 * 
	 * @param s
	 * @return
	 */
	public static String setActionId4Struts(String s) {
		return StringUtil.setFirstCharLowcase(s);
	}
}
