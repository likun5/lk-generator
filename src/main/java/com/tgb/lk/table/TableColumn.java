package com.tgb.lk.table;

import com.tgb.lk.annotation.ExcelVOAttribute;

public class TableColumn {
	@ExcelVOAttribute(name = "序号", column = "A")
	private int id;
	@ExcelVOAttribute(name = "字段名", column = "B")
	private String columnName;
	@ExcelVOAttribute(name = "字段类型", column = "C")
	private String columnType;
	@ExcelVOAttribute(name = "字段长度", column = "D")
	private long length;
	@ExcelVOAttribute(name = "是否允许为空", column = "E")
	private boolean nullable;
	@ExcelVOAttribute(name = "是否主键", column = "F")
	private boolean isKey;
	@ExcelVOAttribute(name = "字段描述", column = "G")
	private String memo;

	@Override
	public String toString() {
		return "TableField [columnName=" + columnName + ", columnType="
				+ columnType + ", id=" + id + ", isKey=" + isKey + ", length="
				+ length + ", memo=" + memo + ", nullable=" + nullable + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public boolean isNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public boolean isKey() {
		return isKey;
	}

	public void setKey(boolean isKey) {
		this.isKey = isKey;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

}
