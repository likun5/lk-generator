package com.tgb.lk.table;

import com.tgb.lk.annotation.AutoField;
import com.tgb.lk.util.ExcelUtil;

import java.io.File;

public class CreateModel {
    private static CreateModel _instance = new CreateModel();
    private String dPath;
    private String fPath;
    private String modelName;
    private String modelPackagePath;

    private CreateModel() {
    }

    public static CreateModel getInstance() {
        return _instance;
    }

    protected void createDictionary() {
        Log.createDictory(dPath);
    }

    public String createModel(String path, Table table) {
        if (table != null) {
            System.out.println("name:" + table.getName().trim());
            dPath = path + File.separator;
            // 去掉全角字符
            String tableName = StringUtil.ruleJavaName(table.getName());
            modelName = NameUtil.setModelName(tableName);
            fPath = dPath + File.separator + modelName + ".java";
            // this.modelPackagePath =
            // NameUtil.setModelPackageName(packagePath);
            createDictionary();
            createHead(table);
            createVariable(table);
            createFunction(table);
        }
        return path;
    }

    public void createHead(Table table) {
        StringBuffer head = new StringBuffer(200);
        if (modelPackagePath != null && !"".equals(modelPackagePath)) {
            head.append("package ").append(modelPackagePath).append(";\n");
        }
        head.append("import java.util.*;\n")
                .append("import java.math.*;\n")
                .append("import com.tgb.lk.annotation.*;\n\n");
        String memo = table.getMemo() == null ? modelName : table.getMemo();
        head.append("@AutoBean(alias = \"" + memo.replace("\"", "'") + "\",table=\""
                + table.getName() + "\")\n");
        head.append("public class ").append(modelName).append(" {\n");
        Log.writeFile(head.toString(), fPath, false);
    }

    public void createVariable(Table table) {
        StringBuffer s = new StringBuffer(200);
        String c[] = table.getColumnName();
        int t[] = table.getColumnType();
        boolean[] nullable = table.getNullable();
        TableColumn[] columns = table.getColumns();
        for (int i = 0; i < c.length; i++) {
            String filedName = c[i].toLowerCase();
            if ("timespan".equals(filedName)) {
                continue;
            }
            String checkField = TableConfig.getInstance().getCheckField();
            if ("true".equals(checkField)) {
                filedName = StringUtil.set_FirstCharUpcase(filedName);
                filedName = filedName.replaceAll("_", "");
            }

            String alias = "";
            if (columns[i].getMemo() != null && columns[i].getMemo().length() > 0) {
                alias = columns[i].getMemo().replace("\"", "'");
            } else {
                if (null != columns[i].getColumnName() && columns[i].getColumnName().length() > 0) {
                    alias = columns[i].getColumnName();
                } else {
                    alias = NameUtil.setPrivateFieldName(filedName);
                }
            }
            s.append("  @AutoField(alias = \"").append(alias).append("\"");

            if (table.getPkName() != null && filedName.toLowerCase().equals(table.getPkName().toLowerCase())) {
                s.append(", isKey = true");
            }
            if (null != columns[i].getColumnName() && columns[i].getColumnName().length() > 0) {
                s.append(", column = \"" + columns[i].getColumnName() + "\"");
            }
            if (null != columns[i].getColumnType() && columns[i].getColumnType().length() > 0) {
                s.append(", columnType=\"").append(columns[i].getColumnType()).append("\"");
            }
            if ((t[i] == 0 || t[i] == 2 || t[i] == 4 || t[i] == 5 || t[i] == 6 || t[i] == 7 || t[i] == 8 || t[i] == 9) && columns[i].getLength() != 0) {
                long length = columns[i].getLength();
                s.append(", length = " + (length < Integer.MAX_VALUE ? length : Integer.MAX_VALUE));
            }
            if (!nullable[i]) {
                s.append(", isRequired = true");
            }
            if (t[i] == 0) {
                s.append(", type = \"").append(AutoField.TYPE_INTEGER).append("\"");
            } else if (t[i] == 1) {
                s.append(", type = \"").append(AutoField.TYPE_DOUBLE).append("\"");
            } else if (t[i] == 2) {
                s.append(", type = \"").append(AutoField.TYPE_STRING).append("\"");
            } else if (t[i] == 3) {
                s.append(", type = \"").append(AutoField.TYPE_DATE).append("\"");
            } else if (t[i] == 4) {
                s.append(", type = \"").append(AutoField.TYPE_BOOLEAN).append("\"");
            } else if (t[i] == 5) {
                s.append(", type = \"").append(AutoField.TYPE_LONG).append("\"");
            } else if (t[i] == 6) {
                s.append(", type = \"").append(AutoField.TYPE_FLOAT).append("\"");
            } else if (t[i] == 7) {
                s.append(", type = \"").append(AutoField.TYPE_BYTE).append("\"");
            } else if (t[i] == 8) {
                s.append(", type = \"").append(AutoField.TYPE_BIGINTEGER).append("\"");
            } else if (t[i] == 9) {
                s.append(", type = \"").append(AutoField.TYPE_BIGDECIMAL).append("\"");
            }
            if ("sex".equals(filedName)) {
                s.append(", combo = {\"男\",\"女\"}");
            }

            s.append(")\n");
            s.append("  @ExcelVOAttribute(name = \"")
                    .append(columns[i].getColumnName()).append("\", column = \"")
                    .append(ExcelUtil.getExcelCol(i) + "\")\n");
            s.append("  private ").append(DataType.DATA_TYPE[t[i]])
                    .append(" ")
                    .append(NameUtil.setPrivateFieldName(filedName))
                    .append(";\n\n");

        }
        // s.append("  private ").append(DataType.DATA_TYPE[t[i]]).append(" ").append(c[i]).append(";\n");
        // s.append("}");
        Log.writeFile(s.toString(), fPath);
    }

    public void createFunction(Table table) {
        StringBuffer s = new StringBuffer(1000);
        String c[] = table.getColumnName();
        int t[] = table.getColumnType();
        // 输出不带参数的构造函数
        // s.append("  public ").append(modelName).append("() {}\n");

        for (int i = 0; i < c.length; i++) {

            String filedName = c[i].toLowerCase();
            if ("timespan".equals(filedName)) {
                continue;
            }
            String checkField = TableConfig.getInstance().getCheckField();
            if ("true".equals(checkField)) {
                filedName = StringUtil.set_FirstCharUpcase(filedName);
                filedName = filedName.replaceAll("_", "");
            }

            s.append("  public ").append(DataType.DATA_TYPE[t[i]]).append(" ")
                    .append(NameUtil.setGetFieldName(filedName))
                    .append("() {\n");
            s.append("    return ")
                    .append(NameUtil.setPrivateFieldName(filedName))
                    .append(";\n  }\n");
            s.append("  public void ")
                    .append(NameUtil.setSetFieldName(filedName)).append("(")
                    .append(DataType.DATA_TYPE[t[i]]).append(" ")
                    .append(NameUtil.setPrivateFieldName(filedName))
                    .append("){\n");
            s.append("    this.")
                    .append(NameUtil.setPrivateFieldName(filedName))
                    .append(" = ")
                    .append(NameUtil.setPrivateFieldName(filedName))
                    .append(";\n  }\n");
        }

        s.append("}");
        Log.writeFile(s.toString(), fPath);
    }

}