package com.tgb.lk.table;

import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.tgb.lk.util.AppConstants;

public class Log {

	public Log() {
	}

	public static void log(int i, int level) {
		log("" + i, level);
	}

	public static void log(String info, int level) {
		Log.createDictory(AppConstants.BASE_PATH + ".log");
		if (level >= TableConfig.getInstance().getLogLevel()) {
            logf(info, level);
        }
	}

	public static void log(String info) {
		log(info, 0);
	}

	public static void log(int info) {
		log(info, 0);
	}

	private static synchronized void logf(String info, int level) {
		infoBuffer.delete(0, infoBuffer.length());
		time.setTime(System.currentTimeMillis());
		infoBuffer.append("[").append(level).append("]")
				.append(time.toString()).append("\t").append(info);
		TableConfig.getInstance().getLogArea().append("\r\n");
		TableConfig.getInstance().getLogArea().append(infoBuffer.toString());
		writeFile(infoBuffer.toString());
	}

	public static void log(Exception e) {
		log(e.getMessage(), 4);
		if (TableConfig.getInstance().getLogLevel() <= 2) {
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(AppConstants.BASE_PATH
						+ ".log/log.txt", true);
				e.printStackTrace(new PrintStream(fos));
			} catch (Exception ex) {
				Log.log(ex);
				ex.printStackTrace();
			} finally {
				if (fos != null) {
                    try {
                        fos.close();
                    } catch (Exception es) {
                        Log.log(es);
                        es.printStackTrace();
                    }
                }
			}
		}
	}

	private static void writeFile(String message) {
		writeFile(message, AppConstants.BASE_PATH + ".log/log.txt");
	}

	public static void writeFile(String message, String path) {
		writeFile(message, path, true);
	}

	public static void writeFile(String message, String path, boolean append) {
		FileWriter os = null;
		try {
			os = new FileWriter(path, append);
			os.write(message + System.getProperty("line.separator"));
		} catch (Exception e) {
			Log.log(e);
			e.printStackTrace();
		} finally {
			try {
				os.close();
			} catch (Exception e) {
				Log.log(e);
				e.printStackTrace();
			}
		}
	}

	public static List readFile(String filename) throws IOException {
		return readFile(filename, 0, 0x7fffffff);
	}

	public static List readFile(String filename, int beginPos, int endPos)
			throws IOException {
		List list = new ArrayList(100);
		BufferedReader reader = null;
		String s = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new BufferedInputStream(new FileInputStream(filename))));
			for (int pos = 0; (s = reader.readLine()) != null
					&& pos >= beginPos && pos++ < endPos;) {
                list.add(s);
            }
			reader.close();
		} catch (IOException e) {
			log(e);
			throw e;
		}
		return list;
	}

	public static void createDictory(String path) {
		File f = new File(path);
		File ft = f;
		for (; !f.exists(); f = f.getParentFile()) {
            ft = f;
        }

		if (f != ft) {
			boolean dirFlag = ft.mkdir();
			createDictory(path);
		}
	}

	public static void copyFile(String oldPathFile, String newPathFile) {
		InputStream inStream = null;
		FileOutputStream fs = null;
		try {
			int byteread = 0;
			File oldfile = new File(oldPathFile);
			if (oldfile.exists()) {
				inStream = new FileInputStream(oldPathFile);
				fs = new FileOutputStream(newPathFile);
				byte buffer[] = new byte[1444];
				while ((byteread = inStream.read(buffer)) != -1) {
                    fs.write(buffer, 0, byteread);
                }
			}
		} catch (Exception e) {
			Log.log(e);
			e.printStackTrace();
		} finally {
			try {
				inStream.close();
			} catch (Exception ex) {
				Log.log(ex);
			}
			try {
				fs.close();
			} catch (Exception ex) {
				Log.log(ex);
			}
		}
	}

	public static final int LEVEL_DEBUG = 0;
	public static final int LEVEL_INFO = 1;
	public static final int LEVEL_ERROR = 2;
	private static Timestamp time = new Timestamp(System.currentTimeMillis());
	private static StringBuffer infoBuffer = new StringBuffer(200);

}