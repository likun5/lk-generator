package com.tgb.lk.table;

import java.io.*;
import java.util.Properties;
import javax.swing.JTextArea;

import com.tgb.lk.util.AppConstants;
import com.tgb.lk.util.FileUtil;

public class TableConfig {
	public static final String proFilePath = AppConstants.BASE_PATH
			+ ".config/config.properties";

	private TableConfig() {
		perNum = 20;
		tableList = new String[0];
		logArea = null;
		dataBaseType = 0;
		loadFromFile(proFilePath);
	}

	public static TableConfig getInstance() {
		return _instance;
	}

	public String getDbConnectString() {
		return dbConnectString;
	}

	public String getDbPasswd() {
		return dbPasswd;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public void setDbPasswd(String dbPasswd) {
		this.dbPasswd = dbPasswd;
	}

	public void setDbConnectString(String dbConnectString) {
		this.dbConnectString = dbConnectString;
	}

	// public String getPackagePath() {
	// return packagePath;
	// }
	//
	// public void setPackagePath(String packagePath) {
	// this.packagePath = packagePath;
	// }

	public int getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}

	public String getStartChar() {
		return startChar;
	}

	public void setStartChar(String startChar) {
		this.startChar = startChar;
	}

	public String getEndChar() {
		return endChar;
	}

	public void setEndChar(String endChar) {
		this.endChar = endChar;
	}

	public void setPath(String path) {
		this.path = path;
		if (path != null && path.lastIndexOf(File.separator) != path.length()) {
			path = path + File.separator;
		}
		String dirPath = AppConstants.BASE_PATH + ".log";
		File dirFile = new File(dirPath);

		if (!dirFile.exists()) {
			if (!dirFile.mkdirs()) {
				throw new RuntimeException(dirFile + "文件夹创建失败");
			}
		}
	}

	public String getPath() {
		if (path != null) {
			path = FileUtil.filterDir(path);
			if (!path.endsWith(File.separator)) {
				path = path + File.separator;
			}
		}
		return path;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public int getPerNum() {
		return perNum;
	}

	public void setPerNum(int perNum) {
		this.perNum = perNum;
	}

	public String[] getTableList() {
		return tableList;
	}

	public void setTableList(String tableList[]) {
		this.tableList = tableList;
	}

	public JTextArea getLogArea() {
		return logArea;
	}

	public void setLogArea(JTextArea logArea) {
		this.logArea = logArea;
	}

	public int getDataBaseType() {
		return dataBaseType;
	}

	public void setDataBaseType(int dataBaseType) {
		this.dataBaseType = dataBaseType;
	}

	public String getMidChar() {
		return midChar;
	}

	public void setMidChar(String midChar) {
		this.midChar = midChar;
	}

	public void loadFromFile(String file) {
		try {
			Properties p = new Properties();
			FileInputStream in = new FileInputStream(new File(file));
			p.load(in);
			loadConfigFromProperties(p);
			in.close();
		} catch (IOException e) {
			this.setDbUsername("root");
			this.setDbPasswd("root");
			this.setDbConnectString("jdbc:mysql://127.0.0.1:3306/mydb");
			// this.setPackagePath("com.tgb.lk.model");
			this.setLogLevel(2);
			this.setPath(AppConstants.BASE_PATH + "dest-beans/");
			this.setSequence("s");
			this.setPerNum(20);
			this.setDataBaseType(AppConstants.DB_MYSQL);
			this.setStartChar("t_");
			this.setEndChar("");
			this.setMidChar("_");
			this.setCheckBean("true");
			this.setCheckField("true");
			// writeToFile(file);
		} catch (Exception e) {
			Log.log(e);
			e.printStackTrace();
		}
	}

	public void writeToFile(String file) {
		try {
			Properties p = new Properties();
			setConfigToProperties(p);

			FileOutputStream stream = new FileOutputStream(new File(file));
			p.store(stream, "autoSSH config");
			stream.close();
		} catch (Exception e) {
			Log.log(e);
			e.printStackTrace();
		}
	}

	public void loadConfigFromProperties(Properties p) {
		dbUsername = p.getProperty("dbUsername");
		dbPasswd = p.getProperty("dbPasswd");
		dbConnectString = p.getProperty("dbConnectString");
		// packagePath = p.getProperty("packagePath");
		path = p.getProperty("path");
		logLevel = Integer.parseInt(p.getProperty("logLevel", "1"));
		sequence = p.getProperty("sequence");
		perNum = Integer.parseInt(p.getProperty("perNum", "0"));
		dataBaseType = Integer.parseInt(p.getProperty("dataBaseType", String
				.valueOf(AppConstants.DB_MYSQL)));
		startChar = p.getProperty("startChar", "t_");
		endChar = p.getProperty("endChar", "");
		midChar = p.getProperty("midChar", "_");
		checkBean = p.getProperty("checkBean", String.valueOf(true));
		checkField = p.getProperty("checkField", String.valueOf(true));
	}

	public void setConfigToProperties(Properties p) {
		p.setProperty("dbUsername", dbUsername);
		p.setProperty("dbPasswd", dbPasswd);
		p.setProperty("dbConnectString", dbConnectString);
		// p.setProperty("packagePath", packagePath);
		if (path != null) {
			p.setProperty("path", path);
		}
		p.setProperty("logLevel", String.valueOf(logLevel));
		p.setProperty("sequence", sequence);
		p.setProperty("perNum", String.valueOf(perNum));
		p.setProperty("dataBaseType", String.valueOf(dataBaseType));
		p.setProperty("startChar", startChar);
		p.setProperty("endChar", endChar);
		p.setProperty("midChar", midChar);
		p.setProperty("checkBean", checkBean);
		p.setProperty("checkField", checkField);
	}

	private static TableConfig _instance = new TableConfig();
	private String dbUsername;
	private String dbPasswd;
	private String dbConnectString;
	// private String packagePath;
	private String path;
	private int logLevel;
	private String sequence;
	private int perNum;
	private String tableList[];
	private JTextArea logArea;
	private int dataBaseType;
	private String startChar;
	private String endChar;
	private String midChar;
	private String checkBean;
	private String checkField;

	public String getCheckBean() {
		return checkBean;
	}

	public void setCheckBean(String checkBean) {
		this.checkBean = checkBean;
	}

	public String getCheckField() {
		return checkField;
	}

	public void setCheckField(String checkField) {
		this.checkField = checkField;
	}

}