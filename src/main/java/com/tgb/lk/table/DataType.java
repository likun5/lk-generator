package com.tgb.lk.table;

import java.util.HashMap;

public class DataType {

    public static final int DATA_INT = 0;
    public static final int DATA_INTEGER = 0;
    public static final int DATA_TINYINT = 0;
    public static final int DATA_SMALLINT = 0;
    public static final int DATA_MEDIUMINT = 0;
    public static final int DATA_DOUBLE = 1;
    public static final int DATA_VARCHAR = 2;
    public static final int DATA_CHAR = 2;
    public static final int DATA_TEXT = 2;
    public static final int DATA_DATE = 3;
    public static final int DATA_TIME = 3;
    public static final int DATA_DATETIME = 3;
    public static final int DATA_TIMESTAMP = 3;
    public static final int DATA_BIT = 4;
    public static final int DATA_BOOLEAN = 4;
    public static final int DATA_BIGINT = 5;
    public static final int DATA_FLOAT = 6;
    public static final int DATA_BLOB = 7;
    public static final int DATA_DECIMAL = 9;

    public static final String DATA_TYPE[] = {"Integer", "Double", "String", "Date", "Boolean", "Long",
            "Float", "byte[]", "BigInteger", "BigDecimal"};
    public static DataType _instance = new DataType();
    private HashMap<String, Integer> _TYPE_MAP;

    private DataType() {
        _TYPE_MAP = new HashMap<String, Integer>();

        //oracle
        _TYPE_MAP.put("bit", Integer.valueOf(4));
        _TYPE_MAP.put("raw", Integer.valueOf(7));
        _TYPE_MAP.put("char", Integer.valueOf(2));
        _TYPE_MAP.put("varchar2", Integer.valueOf(2));
        _TYPE_MAP.put("number", Integer.valueOf(0));
        _TYPE_MAP.put("numberf", Integer.valueOf(1));
        _TYPE_MAP.put("longtext", Integer.valueOf(2));
        _TYPE_MAP.put("integer", Integer.valueOf(0));
        _TYPE_MAP.put("blob", Integer.valueOf(7));
        _TYPE_MAP.put("clob", Integer.valueOf(7));
        _TYPE_MAP.put("nclob", Integer.valueOf(7));
        _TYPE_MAP.put("timestamp", Integer.valueOf(3));
        _TYPE_MAP.put("date", Integer.valueOf(3));

        //sqlServer,参考http://blog.csdn.net/lg312200538/article/details/5993049
        _TYPE_MAP.put("int", Integer.valueOf(0));
        _TYPE_MAP.put("varchar", Integer.valueOf(2));
        _TYPE_MAP.put("char", Integer.valueOf(2));
        _TYPE_MAP.put("nchar", Integer.valueOf(2));
        _TYPE_MAP.put("nvarchar", Integer.valueOf(2));
        _TYPE_MAP.put("text", Integer.valueOf(2));
        _TYPE_MAP.put("ntext", Integer.valueOf(2));
        _TYPE_MAP.put("tinyint", Integer.valueOf(0));
        _TYPE_MAP.put("int", Integer.valueOf(0));
        _TYPE_MAP.put("tinyint", Integer.valueOf(0));
        _TYPE_MAP.put("smallint", Integer.valueOf(0));
        _TYPE_MAP.put("bit", Integer.valueOf(4));
        _TYPE_MAP.put("bigint", Integer.valueOf(5));
        _TYPE_MAP.put("float", Integer.valueOf(6));
        _TYPE_MAP.put("decimal", Integer.valueOf(9));
        _TYPE_MAP.put("money", Integer.valueOf(9));
        _TYPE_MAP.put("smallmoney", Integer.valueOf(9));
        _TYPE_MAP.put("numeric", Integer.valueOf(9));
        _TYPE_MAP.put("real", Integer.valueOf(6));
        _TYPE_MAP.put("smalldatetime", Integer.valueOf(3));
        _TYPE_MAP.put("datetime", Integer.valueOf(3));
        _TYPE_MAP.put("timestamp", Integer.valueOf(3));
        _TYPE_MAP.put("binary", Integer.valueOf(7));
        _TYPE_MAP.put("varbinary", Integer.valueOf(7));
        _TYPE_MAP.put("image", Integer.valueOf(7));


        //mysql对应关系表,参考http://www.cnblogs.com/jerrylz/p/5814460.html整理
        _TYPE_MAP.put("varchar", Integer.valueOf(2));
        _TYPE_MAP.put("char", Integer.valueOf(2));
        _TYPE_MAP.put("blob", Integer.valueOf(7));
        _TYPE_MAP.put("text", Integer.valueOf(2));
        _TYPE_MAP.put("int", Integer.valueOf(0));
        _TYPE_MAP.put("integer", Integer.valueOf(0));
        _TYPE_MAP.put("tinyint", Integer.valueOf(0));
        _TYPE_MAP.put("smallint", Integer.valueOf(0));
        _TYPE_MAP.put("mediumint", Integer.valueOf(0));
        _TYPE_MAP.put("bit", Integer.valueOf(4));
        _TYPE_MAP.put("bigint", Integer.valueOf(5));
        _TYPE_MAP.put("float", Integer.valueOf(6));
        _TYPE_MAP.put("double", Integer.valueOf(1));
        _TYPE_MAP.put("decimal", Integer.valueOf(9));
        _TYPE_MAP.put("boolean", Integer.valueOf(4));
        _TYPE_MAP.put("date", Integer.valueOf(3));
        _TYPE_MAP.put("time", Integer.valueOf(3));
        _TYPE_MAP.put("datetime", Integer.valueOf(3));
        _TYPE_MAP.put("timestamp", Integer.valueOf(3));

    }

    public static DataType getInstance() {
        return _instance;
    }

    public static String getDescription(int dataType) {
        if (dataType >= 3 || dataType < 0) {
            return "";
        } else {
            return DATA_TYPE[dataType];
        }
    }

    public int getTypeMap(String type) {
        Integer r = (Integer) _TYPE_MAP.get(type);
        if (r != null) {
            return r.intValue();
        } else {
            return 2;
        }
    }

}