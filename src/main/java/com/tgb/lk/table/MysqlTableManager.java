package com.tgb.lk.table;

import java.sql.*;
import javax.sql.rowset.CachedRowSet;

public class MysqlTableManager extends TableManager {

	private MysqlTableManager() {
	}

	public static TableManager getInstance() {
		return _instance;
	}

	@Override
	public Table getTableInfo(String name) {
		Table table = new Table();
		table.setName(StringUtil.ruleJavaName(name));
		TableConfig config = TableConfig.getInstance();
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		String dbString = config.getDbConnectString();
		String tableSchema = "";
		if (dbString != null & dbString.length() > 0) {
			tableSchema = dbString.substring(dbString.lastIndexOf('/') + 1,
					dbString.length());
		}

		try {
			conn = DBManager.getInstance().getConnection(
					config.getDbConnectString(), config.getDbUsername(),
					config.getDbPasswd());
			st = conn.createStatement(1005, 1007);
			String sql = "select column_name, data_type, t.is_nullable ,t.character_maximum_length length, t.column_comment memo "
					+ "from information_schema.columns t Where table_name = '"
					+ name
					+ "' and table_schema ='"
					+ tableSchema + "'";
			rs = st.executeQuery(sql);
			int num;
			for (num = 0; rs.next(); num++) {
				;
			}
			rs.beforeFirst();
			table.setColumnNum(num);
			TableColumn[] columns = new TableColumn[num];
			String tmp = "";
			String c[] = new String[num];
			int t[] = new int[num];
			boolean[] nullable = new boolean[num];
			int i = 0;
			while (rs.next()) {
				c[i] = rs.getString("column_name").trim();
				tmp = rs.getString("data_type").trim();
				nullable[i] = ("NO".equals(rs.getString("is_nullable"))) ? false
						: true;

				TableColumn column = new TableColumn();
				column.setId(i + 1);
				column.setColumnName(c[i]);
				column.setColumnType(tmp);
				column.setNullable(nullable[i]);
				column.setKey(false);
				column.setLength(rs.getLong("length"));
				column.setMemo(rs.getString("memo"));
				columns[i] = column;

				t[i++] = DataType.getInstance().getTypeMap(tmp);
			}
			table.setColumnName(c);
			table.setColumnType(t);
			table.setNullable(nullable);
			table.setColumns(columns);
			DBManager.getInstance().cleanup(null, null, rs);
			rs = st.executeQuery("select column_name from information_schema.columns t Where table_name = '"
					+ name
					+ "' and column_key = 'PRI'  and table_schema = '"
					+ tableSchema + "'");
			String[] keys = getKey(rs);
			int[] keyPos = getKeyPos(c, getKey(rs));
			table.setKeyPos(keyPos);
			for (int j : keyPos) {
				columns[j].setKey(true);
			}
			if (keys != null && keys.length > 0) {
				table.setPkName(keys[0]);
			}

			DBManager.getInstance().cleanup(null, null, rs);
			rs = st.executeQuery("SELECT table_comment FROM INFORMATION_SCHEMA.tables WHERE table_name = '"
					+ name
					+ "' AND table_schema = '"
					+ tableSchema + "'");
			while (rs.next()) {
				String memo = rs.getString("table_comment").trim();
				table.setMemo(memo);
			}
		} catch (Exception e) {
			Log.log(e);
		} finally {
			DBManager.getInstance().cleanup(conn, st, rs);
		}
		return table;
	}

	@Override
	public String[] getAllTableName() {
		String dbConnStr = TableConfig.getInstance().getDbConnectString();
		String dbName = dbConnStr.substring(dbConnStr.lastIndexOf("/") + 1);
		String tables[] = null;
		String sql = "select table_name from information_schema.`TABLES` t where TABLE_SCHEMA = '"
				+ dbName + "'";
		CachedRowSet crs = DBManager.getInstance().executeQuery(sql, null);
		try {
			tables = new String[crs.size()];
			int i = 0;
			while (crs.next()) {
				tables[i++] = crs.getString("table_name").toLowerCase();
			}
		} catch (Exception e) {
			Log.log(e);
		}
		return tables;
	}

	private static MysqlTableManager _instance = new MysqlTableManager();

}