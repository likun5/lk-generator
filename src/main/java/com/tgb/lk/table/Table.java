package com.tgb.lk.table;

public class Table {
	private String name;
	private int columnNum;
	private String columnName[];
	private int columnType[];
	private boolean nullable[];
	int keyPos[];
	private String pkName;// 主键名称
	private TableColumn[] columns;
	private String memo;

	public String getPkName() {
		return pkName;
	}

	public void setPkName(String pkName) {
		this.pkName = pkName;
	}

	public Table() {
	}

	public int getColumnNum() {
		return columnNum;
	}

	public String[] getColumnName() {
		return columnName;
	}

	public int[] getColumnType() {
		return columnType;
	}

	public int[] getKeyPos() {
		return keyPos;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setKeyPos(int keyPos[]) {
		this.keyPos = keyPos;
	}

	public void setColumnType(int columnType[]) {
		this.columnType = columnType;
	}

	public void setColumnNum(int columnNum) {
		this.columnNum = columnNum;
	}

	public void setColumnName(String columnName[]) {
		this.columnName = columnName;
	}

	public boolean[] getNullable() {
		return nullable;
	}

	public void setNullable(boolean[] nullable) {
		this.nullable = nullable;
	}

	public TableColumn[] getColumns() {
		return columns;
	}

	public void setColumns(TableColumn[] columns) {
		this.columns = columns;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}