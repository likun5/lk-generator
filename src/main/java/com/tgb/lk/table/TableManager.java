package com.tgb.lk.table;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TableManager {

	public TableManager() {
	}

	public Table getTableInfo(String name) {
		return null;
	}

	protected String[] getKey(ResultSet rs) throws SQLException {
		String r[] = null;
		int i;
		for (i = 0; rs.next(); i++) {
            ;
        }
		r = new String[i];
		while (rs.previous()) {
            r[--i] = rs.getString("column_name").toLowerCase();
        }
		return r;
	}

	protected int[] getKeyPos(String cs[], String uniques[]) {
		if (cs == null || uniques == null) {
            return new int[0];
        }
		int pos[] = new int[uniques.length];
		for (int i = 0; i < uniques.length; i++) {
			for (int j = 0; j < cs.length; j++) {
				if (!cs[j].equals(uniques[i])) {
                    continue;
                }
				pos[i] = j;
				break;
			}

		}

		return pos;
	}

	public String[] getAllTableName() {
		return null;
	}
}