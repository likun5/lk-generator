package com.tgb.lk.table;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

    private StringUtil() {
    }

    public static String setFirstCharUpcase(String s) {
        if (s == null || s.length() < 1) {
            return s;
        }
        char c[] = s.toCharArray();
        if (c.length > 0 && c[0] >= 'a' && c[0] <= 'z') {
            c[0] = (char) ((short) c[0] - 32);
        }
        return String.valueOf(c);
    }

    public static String setFirstCharLowcase(String s) {
        if (s == null || s.length() < 1) {
            return s;
        }
        char c[] = s.toCharArray();
        if (c.length > 0 && c[0] >= 'A' && c[0] <= 'Z') {
            c[0] = (char) ((short) c[0] + 32);
        }
        return String.valueOf(c);
    }

    public static String set_FirstCharUpcase(String s) {
        if (s == null || s.length() < 1) {
            return s;
        }
        char c[] = s.toCharArray();
        boolean flag = false;
        for (int i = 0; i < c.length; i++) {
            if (flag && c[i] >= 'a' && c[i] <= 'z') {
                c[i] = (char) ((short) c[i] - 32);
                flag = false;
            }
            if (!flag && c[i] == '_') {
                flag = true;
            }
        }
        return String.valueOf(c);
    }

    /**
     * 去除字符串包含的所有空格（包括:空格(全角，半角)、制表符、换页符等）
     *
     * @param s
     * @return
     */
    public static String removeAllBlank(String s) {
        String result = "";
        if (null != s && !"".equals(s)) {
            result = s.replaceAll("[　*| *| *|//s*]*", "");
        }
        return result;
    }

    /**
     * 去除字符串中头部和尾部所包含的空格（包括:空格(全角，半角)、制表符、换页符等）  * @param s  * @return
     */
    public static String trim(String s) {
        String result = "";
        if (null != s && !"".equals(s)) {
            result = s.replaceAll("^[　*| *| *|//s*]*", "").replaceAll("[　*| *| *|//s*]*$", "");
        }
        return result;
    }

    /**
     * java 标识符可以是 字母、数字、$、_(下划线)，
     * todo:不可用数字开头
     * @param name
     * @return
     */
    public static String ruleJavaName(String name){
        name = name.replaceAll("[^0-9a-zA-Z_]","");
        System.out.println(name);
        return name;
    }

}