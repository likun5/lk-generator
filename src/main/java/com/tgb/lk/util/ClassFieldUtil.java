package com.tgb.lk.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ClassFieldUtil {
	/**
	 * 得到实体类所有通过注解映射了数据表的字段
	 * 
	 * @param map
	 * @return
	 */
	public static List<Field> getMappedField(Class<?> clazz,
			List<Field> fields, Class<? extends Annotation> annotateCls) {
		if (fields == null) {
			fields = new ArrayList<Field>();
		}

		Field[] allFields = clazz.getDeclaredFields();// 得到所有定义字段
		// 得到所有field并存放到一个list中.
		for (Field field : allFields) {
			if (field.isAnnotationPresent(annotateCls)) {
				fields.add(field);
			}
		}
		if (clazz.getSuperclass() != null
				&& !clazz.getSuperclass().equals(Object.class)) {
			getMappedField(clazz.getSuperclass(), fields, annotateCls);
		}

		return fields;
	}

	/**
	 * 得到实体类所有通过注解映射了数据表的字段
	 * 
	 * @param map
	 * @return
	 */
	public static Map<String, Field> getMappedField(Class<?> clazz,
			Map<String, Field> map, Class<? extends Annotation> annotateCls) {
		if (map == null) {
			map = new LinkedHashMap<String, Field>();
		}
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(annotateCls)) {
				map.put(field.getName(), field);
			}
		}
		if (clazz.getSuperclass() != null
				&& !clazz.getSuperclass().equals(Object.class)) {
			getMappedField(clazz.getSuperclass(), map, annotateCls);
		}
		return map;
	}

	/**
	 * 得到实体类所有通过注解映射了数据表的字段
	 * 
	 * @param map
	 * @return
	 */
	public static Map<String, Field> getMappedField(Class<?> clazz,
			Map<String, Field> map) {
		if (map == null) {
			map = new LinkedHashMap<String, Field>();
		}
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			map.put(field.getName(), field);
		}
		if (clazz.getSuperclass() != null
				&& !clazz.getSuperclass().equals(Object.class)) {
			getMappedField(clazz.getSuperclass(), map);
		}
		return map;
	}

	/**
	 * 得到实体类所有通过注解映射了数据表的字段
	 * 
	 * @param map
	 * @return
	 */
	public static List<Field> getMappedField(Class<?> clazz, List<Field> fields) {
		if (fields == null) {
			fields = new ArrayList<Field>();
		}

		Field[] allFields = clazz.getDeclaredFields();// 得到所有定义字段
		// 得到所有field并存放到一个list中.
		for (Field field : allFields) {
			fields.add(field);
		}
		if (clazz.getSuperclass() != null
				&& !clazz.getSuperclass().equals(Object.class)) {
			getMappedField(clazz.getSuperclass(), fields);
		}

		return fields;
	}
}
