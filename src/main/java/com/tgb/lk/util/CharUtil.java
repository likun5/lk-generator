package com.tgb.lk.util;

public class CharUtil {
	// 首字母转大写
	public static String toUpperCaseFirstOne(String s) {
		if (s != null && s.length() > 0) {
			if (Character.isUpperCase(s.charAt(0))) {
				return s;
			} else {
				return (new StringBuilder()).append(
						Character.toUpperCase(s.charAt(0))).append(
						s.substring(1)).toString();
			}
		}
		return s;
	}

	// 首字母转小写
	public static String toLowerCaseFirstOne(String s) {
		if (s != null && s.length() > 0) {
			if (Character.isLowerCase(s.charAt(0))) {
				return s;
			} else {
				return (new StringBuilder()).append(
						Character.toLowerCase(s.charAt(0))).append(
						s.substring(1)).toString();
			}
		}
		return s;
	}
}
