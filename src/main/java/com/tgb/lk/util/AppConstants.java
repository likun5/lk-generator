package com.tgb.lk.util;

public class AppConstants {
    public static final String APP_VERSION = "V1.3.8";
    public static final String APP_COPYRIGHT = "Copyright @2018  All Rights Reserved";
    public static final String APP_AUTHOR_CONTECT = "联系方式：LK123@126.com";

    public static final String BASE_PATH = "C:\\.LKGenerator\\";
    public static final String HELP_PATH = BASE_PATH + ".help";
    public static final String BEANS_PATH = BASE_PATH + "beans";
    public static final int DB_MYSQL = 1;
    public static final int DB_SQLSERVER = 2;
    public static final int DB_ORACLE = 3;

}
