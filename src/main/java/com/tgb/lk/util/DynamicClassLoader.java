package com.tgb.lk.util;

/**
 * @author vma
 */
// 自定义一个类加载器
public class DynamicClassLoader extends ClassLoader {

	public Class<?> findClass(byte[] b) throws ClassNotFoundException {

		return defineClass(null, b, 0, b.length);
	}

}
