package com.tgb.lk.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.tgb.lk.table.Log;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.DomDriver;

/*
 * 封装后的读写配置文件的工具类,可以将设置注解后的对象与xml转化. 
 * 写入用saveConfig方法,读取用getConfig方法
 * 
 * @author 李坤
 * 
 * 使用步骤: 
 * 1.引入工具类和xstream.jar
 * 2.写一个类或几个类并配置xstream注解,用于对象和xml之间的转换.注意,一定要在需转化的实体类上设置注解.
 * 3.写一个子类继承XmlUtil类,并且实现抽象方法.
 * 4.文件名,可以用../代表上级文件夹路径.
 * 5.setDefaultConfig方法,本方法的作用是当配置文件找不到时自动生成一个默认的配置文件,提高程序的健壮性.
 * 6.修改main方法可进行测试,写入用saveConfig方法,读取用getConfig方法.
 * 7.还可以加一些自己的逻辑处理自己的业务的方法.
 */
public abstract class XmlUtil<T> {
	// 创建一个默认的配置文件,需重写
	public abstract T setDefaultConfig();

	private String file;
	private Class<T> clazz;

	public XmlUtil(Class<T> clazz, String file) {
		this.clazz = clazz;
		String dirPath = file.substring(0, file.lastIndexOf("/"));
		File dirFile = new File(dirPath);

		if (!dirFile.exists()) {
			if (!dirFile.mkdirs()) {
				throw new RuntimeException(dirFile + "文件夹创建失败");
			}
		}
		this.file = file;
	}

	/**
	 *保存xml文件
	 * 
	 * @param entity
	 *            xml文件对应的对象
	 */
	public void saveConfig(T entity) {
		System.out.println("===保存配置====");
		XStream stream = new XStream(new DomDriver("utf-8"));// xml文件使用utf-8格式
		stream.autodetectAnnotations(true);// 设置自动匹配annotation.
		try {
			FileOutputStream out = new FileOutputStream(file);
			stream.toXML(entity, out);// 将实体类转为xml并输出到文件.
		} catch (FileNotFoundException e) {
			Log.log(e);
			e.printStackTrace();
		}
	}

	/**
	 * 从配置文件中读取配置,并自动转换为对应的对象.
	 * 
	 * @return T
	 */
	@SuppressWarnings("unchecked")
	public T getConfig() {
		XStream stream = new XStream(new DomDriver("utf-8"));// xml文件使用utf-8格式
		stream.autodetectAnnotations(true);// 设置自动匹配annotation.
		FileInputStream input;
		T entity;
		try {
			input = new FileInputStream(file);
			String alias = "";
			if (clazz.isAnnotationPresent(XStreamAlias.class)) {
				alias = clazz.getAnnotation(XStreamAlias.class).value();
			}
			stream.alias(alias, clazz);// 由于xstream-1.3.1.jar中有个bug,从xml文件中读出的内容autodetectAnnotations(true)不生效,必须用alias后才正常.
			entity = (T) stream.fromXML(input); // 从配置文件中读取配置,并自动转换为对应的对象
		} catch (FileNotFoundException e1) {
			Log.log(e1);
			entity = setDefaultConfig();// 文件不存在时创建一个默认的配置文件.
			if (entity != null) {
				saveConfig(entity);
			}
		}
		return entity;
	}
}
