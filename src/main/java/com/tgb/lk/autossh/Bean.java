package com.tgb.lk.autossh;

import java.util.List;

import com.tgb.lk.util.CharUtil;

public class Bean {

	private String name;
	private BeanField[] fields;
	private String key;
	private String alias;
	private String table;
	private String[] args;
	private List<Object> datas;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUname() {
		return CharUtil.toUpperCaseFirstOne(name);
	}

	public String getLname() {
		return CharUtil.toLowerCaseFirstOne(name);
	}

	public String getAUname() {
		return name.toUpperCase();
	}

	public String getALname() {
		return name.toLowerCase();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public BeanField[] getFields() {
		return fields;
	}

	public void setFields(BeanField[] fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		return getName();
	}

	public String getAlias() {
		if (alias == null || "".equals(alias)) {
			return this.name;
		}
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

	public int getArgsLength() {
		if (this.args != null) {
			return this.args.length;
		}
		return 0;
	}

	public int getArgsSize() {
		return getArgsLength();
	}

	public List<Object> getDatas() {
		return datas;
	}

	public void setDatas(List<Object> datas) {
		this.datas = datas;
	}

	public int getDatasLength() {
		if (this.datas != null) {
			return this.datas.size();
		}
		return 0;
	}

	public int getDatasSize() {
		return getDatasLength();
	}

	public int getFieldsLength() {
		if (this.fields != null) {
			return this.fields.length;
		}
		return 0;
	}

	public int getFieldsSize() {
		return getFieldsLength();
	}
}
