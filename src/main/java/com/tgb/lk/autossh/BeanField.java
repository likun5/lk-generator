package com.tgb.lk.autossh;

import com.tgb.lk.util.CharUtil;

public class BeanField {
    private String name;
    private String alias;
    private String type;
    private String column;
    private String columnType;
    private boolean isKey;
    private boolean isRequired;
    private String[] combo;
    private String[] args;
    private int length;

    public String getName() {
        return name;
    }

    /*
     * 首字符大写
     */
    public String getUname() {
        return CharUtil.toUpperCaseFirstOne(name);
    }

    /*
     * 首字符小写
     */
    public String getLname() {
        return CharUtil.toLowerCaseFirstOne(name);
    }

    /*
     * 全部大写
     */
    public String getAUname() {
        return name.toUpperCase();
    }

    /*
     * 全部小写
     */
    public String getALname() {
        return name.toLowerCase();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        if (alias == null || "".equals(alias)) {
            return this.name;
        }
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getIsKey() {
        return isKey;
    }

    public void setKey(boolean isKey) {
        this.isKey = isKey;
    }

    public boolean getIsRequired() {
        return isRequired;
    }


    public void setRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public String[] getCombo() {
        return combo;
    }

    public void setCombo(String[] combo) {
        this.combo = combo;
    }

    public int getComboLength() {
        if (this.combo != null) {
            return this.combo.length;
        }
        return 0;
    }

    public int getComboSize() {
        return getComboLength();
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public int getArgsLength() {
        if (this.args != null) {
            return this.args.length;
        }
        return 0;
    }

    public int getArgsSize() {
        return getArgsLength();
    }

    public String getColumn() {
        return column;
    }

    /*
     * 首字符小写
     */
    public String getLcolumn() {
        return CharUtil.toUpperCaseFirstOne(column);
    }

    /*
     * 首字符大写
     */
    public String getUcolumn() {
        return CharUtil.toUpperCaseFirstOne(column);
    }

    /*
     * 全部小写
     */
    public String getALcolumn() {
        return column.toLowerCase();
    }

    /*
     * 全部大写
     */
    public String getAUcolumn() {
        return column.toUpperCase();
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getAUcolumnType(){
        return columnType.toUpperCase();
    }

    public String getALcolumnType(){
        return columnType.toLowerCase();
    }

    public String getUcolumnType(){
        return CharUtil.toUpperCaseFirstOne(column);
    }

    public String getLcolumnType(){
        return CharUtil.toLowerCaseFirstOne(column);
    }


    public boolean isKey() {
        return isKey;
    }

    public boolean isRequired() {
        return isRequired;
    }
}
