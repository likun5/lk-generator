package com.tgb.lk.model;

import java.util.*;
import javax.persistence.*;
import com.tgb.lk.util.base.model.BaseTimeModel;
import com.tgb.lk.util.excel.ExcelVOAttribute;

@Entity
@Table(name = "t_student")
public class Student extends BaseTimeModel{
	

	@Column(name = "id", nullable = false)
	@ExcelVOAttribute(name = "序号", column = "B")
	private String id;

	@Column(name = "name", nullable = false)
	@ExcelVOAttribute(name = "姓名", column = "C")
	private String name;

	@Column(name = "sex")
	@ExcelVOAttribute(name = "性别", column = "D")
	private String sex;

	@Column(name = "age")
	@ExcelVOAttribute(name = "年龄", column = "E")
	private Integer age;

	@Column(name = "birthday")
	@ExcelVOAttribute(name = "生日", column = "F")
	private Date birthday;

	@Column(name = "isMonitor")
	@ExcelVOAttribute(name = "是否班长", column = "G")
	private Boolean isMonitor;

	@Column(name = "createTime", nullable = false)
	@ExcelVOAttribute(name = "创建时间", column = "H")
	private Date createTime;

	@Column(name = "updateTime", nullable = false)
	@ExcelVOAttribute(name = "修改时间", column = "I")
	private Date updateTime;


	public Student() {
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Boolean getIsMonitor() {
		return isMonitor;
	}

	public void setIsMonitor(Boolean isMonitor) {
		this.isMonitor = isMonitor;
	}
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
