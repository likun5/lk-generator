package com.tgb.lk.web.action;

import java.util.*;

import com.tgb.lk.util.base.action.*;
import com.tgb.lk.util.excel.ExcelUtil;
import com.tgb.lk.util.query.*;

import com.tgb.lk.model.*;
import com.tgb.lk.service.*;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class StudentAction extends BaseAction{

	public StudentAction() {
	}

	private Student student;

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	

	private StudentService studentService;

	public void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}

	public String add() {
		if (student != null) {
			studentService.save(student);
		}
		return "add";
	}

	public String showUpdate() {
		if (!id.equals("")) {
			student = studentService.find(id);
		}
		return "showUpdate";
	}

	public String update() {
		if (student != null) {
			studentService.update(student);
		}
		return "update";
	}

	public String delete() {
		String ids = args.get("ids");
		String[] strIds = ids.split(",");
		studentService.delete(strIds);
		return "delete";
	}


	public String list() {
		List<Student> list = new ArrayList<Student>();
		result = studentService.query();
		list = result.getResult();
		return "list";
	}

	public String find() {
		if (!id.equals("")) {
			student = studentService.find(id);
		}
		return "find";
	}
	
	public String export() {
		return "export";
	}

	
}
