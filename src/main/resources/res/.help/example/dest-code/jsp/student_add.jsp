<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@include file="/jsp/common/common.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>student_add</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="<%=basePath%>style/crm.css" rel="stylesheet"
			type="text/css">
		<style type="text/css"></style>
		<script type="text/javascript" language="javascript"
			src="<%=basePath%>script/public.js">
</script>
		<script type="text/javascript" language="javascript"
			src="<%=basePath%>script/My97DatePicker/WdatePicker.js">
</script>
		<script type="text/javascript" language="javascript">
function submit2() {
		if(checkNullStr(document.getElementById("student.id"),"[序号]字段不能为空!")){
		return;
	}
				if(checkNullStr(document.getElementById("student.name"),"[姓名]字段不能为空!")){
		return;
	}
							if (!checkNum(document.getElementById("student.age"),"[年龄]字段请输入数字")) {
		return;
	}
							if(checkNullStr(document.getElementById("student.createTime"),"[创建时间]字段不能为空!")){
		return;
	}
				if(checkNullStr(document.getElementById("student.updateTime"),"[修改时间]字段不能为空!")){
		return;
	}
			with (document.getElementById("myform")) {
		submit();
	}
}
</script>
	</head>
	<body>
		<center>
		<font color="red"><s:fielderror/></font>
			<form action="student_add.action" method="post" name="myform"
				id="myform">
				<table class="tableEdit" cellspacing="1" cellpadding="0">
					<tbody>
						<tr>
							<!-- 这里是添加、编辑界面的标题 -->
							<td align="center" class="tdEditTitle">
								添加student信息
							</td>
						</tr>
						<tr>
							<td>
								<!-- 主输入域开始 -->
								<table class="tableEdit" cellspacing="0" cellpadding="0">
									 																		<tr>
																			<td class="tdEditLabel">
											序号
										</td>
																																	<td class="tdEditContent">
													<input type="text" name="student.id"
														id="student.id">
												</td>
																														 																			<td class="tdEditLabel">
											姓名
										</td>
																																	<td class="tdEditContent">
													<input type="text" name="student.name"
														id="student.name">
												</td>
																																							</tr>
									 																		<tr>
																			<td class="tdEditLabel">
											性别
										</td>
																																	<td class="tdEditContent">
													<select name="student.sex" id="student.sex">
																											<option value="男">
														男
														</option>
																											<option value="女">
														女
														</option>
																										</select>
												</td>
																														 																			<td class="tdEditLabel">
											年龄
										</td>
																																	<td class="tdEditContent">
													<input type="text" name="student.age"
														id="student.age">
												</td>
																																							</tr>
									 																		<tr>
																			<td class="tdEditLabel">
											生日
										</td>
																					<td class="tdEditContent">
												<input type="text" name="student.birthday"
													id="student.birthday" onfocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})">
											</td>
																			 																			<td class="tdEditLabel">
											是否班长
										</td>
																																	<td class="tdEditContent">
													<input type="text" name="student.isMonitor"
														id="student.isMonitor">
												</td>
																																							</tr>
									 																		<tr>
																			<td class="tdEditLabel">
											创建时间
										</td>
																					<td class="tdEditContent">
												<input type="text" name="student.createTime"
													id="student.createTime" onfocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})">
											</td>
																			 																			<td class="tdEditLabel">
											修改时间
										</td>
																					<td class="tdEditContent">
												<input type="text" name="student.updateTime"
													id="student.updateTime" onfocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})">
											</td>
																												</tr>
									  								</table>
								<!-- 主输入域结束 -->
							</td>
						</tr>
					</tbody>
				</table>

				<table style="width: 100%">
					<tr align="center">
						<td colspan="3">
							<input class="MyButton" type="button" name="saveButton"
								value="保存信息" onclick="submit2();">
							<input class="MyButton" type="button" name="closeButton"
								value="关闭窗口" onclick="window.close();">
						</td>
					</tr>
				</table>
			</form>
		</center>
	</body>
</html>

