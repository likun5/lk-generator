<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@include file="/jsp/common/common.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>student_list</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="<%=basePath%>style/crm.css" rel="stylesheet"
			type="text/css">
		<style type="text/css"></style>
		<script type="text/javascript" language="javascript"
			src="<%=basePath%>script/public.js">
</script>
		<script type="text/javascript" language="javascript">
</script>
	</head>
	<body>
		<center>
			<table class="t1" cellpadding="0" cellspacing="0">
				<tbody>
					<tr class="t1_tr1">
						<td class="t1_tr1_td1"></td>
						<td class="t1_tr1_td2">
							<span>student管理</span>
						</td>
						<td class="t1_tr1_td3"></td>
						<td class="t1_tr1_td4">
							<span></span>&nbsp;
							<!--此处不可去掉span标签,去掉后IE中显示出问题. -->
						</td>
					</tr>
				</tbody>
			</table>
			<table class="t2" cellpadding="0" cellspacing="0">
				<tbody>
					<tr class="t2_tr1">
						<td class="t2_tr1_td1"></td>
						<td class="t2_tr1_td2"></td>
						<td class="t2_tr1_td3"></td>
					</tr>
					<tr class="t2_tr2">
						<td class="t2_tr2_td1">
							<!-- 在这里插入查询表单 -->
						</td>
						<td class="t2_tr2_td2">
							<%-- 
            <!--在这里定义“添加”，“查询”等按钮-->
             <input type="image" name="find" value="find" src="images/cz.gif">
             <a href="#" onclick="BeginOut('document.do?method=addInput','470')"><img alt="" src="images/addpic.gif" border=0 align="middle" style="CURSOR: hand"></a>
            --%>
							<a href="#"
								onclick="openWin('student_showAdd.action','addstudent',750,400);">添加信息</a>
						</td>
						<td class="t2_tr2_td3"></td>
					</tr>
				</tbody>
			</table>
			<table class="t3" cellpadding="0" cellspacing="0">
				<tr class="t3_tr1">
					<td class="t3_tr1_td1"></td>
					<td class="t3_tr1_td2"></td>
					<td class="t3_tr1_td3">
						<!-- 可以在这里插入分页导航条 -->
					</td>
				</tr>
			</table>
			<table class="t4" cellpadding="0" cellspacing="1">
				<!-- 列表标题栏 -->
				<tr class="t4_tr1" bgcolor="#EFF3F7" class="TableBody1">
					<td class="show" width="10%" align="center">
						<b>操作</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>序号</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>姓名</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>性别</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>年龄</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>生日</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>是否班长</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>创建时间</b>
					</td>
										<td class="show" width="4%" align="center">
						<b>修改时间</b>
					</td>
									</tr>
				<!-- 列表数据栏 -->
				<c:choose>
					<c:when test="${!empty pm.datas}">
						<c:forEach items="${pm.datas}" var="student">
							<tr class="t4_tr" bgcolor="#EFF3F7" class="TableBody1"
								onmouseover="this.bgcolor = '#DEE7FF';"
								onmouseout="this.bgcolor='#EFF3F7';">
								<td class="show" align="center" valign="middle">
									<a href="#"
										onclick="openWin('student_showUpdate.action?id=${student.id}','showUpdate',750,400)">修改</a>
									<a href="#"
										onclick="del('student_delete.action?id=${student.id }');">删除</a>
								</td>
																<td class="show" align="center" valign="middle">
									${student.id}
								</td>
																<td class="show" align="center" valign="middle">
									${student.name}
								</td>
																<td class="show" align="center" valign="middle">
									${student.sex}
								</td>
																<td class="show" align="center" valign="middle">
									${student.age}
								</td>
																<td class="show" align="center" valign="middle">
									${student.birthday}
								</td>
																<td class="show" align="center" valign="middle">
									${student.isMonitor}
								</td>
																<td class="show" align="center" valign="middle">
									${student.createTime}
								</td>
																<td class="show" align="center" valign="middle">
									${student.updateTime}
								</td>
															</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<!-- 在列表数据为空的时候，要显示的提示信息 -->
						<tr>
							<td colspan="21" align="center" bgcolor="#EFF3F7"
								class="TableBody1" onmouseover="this.bgcolor = '#DEE7FF';"
								onmouseout="this.bgcolor='#EFF3F7';">
								没有找到相应的记录
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</table>
			<%@include file="/jsp/common/pager.jsp"%>
		</center>
	</body>
</html>

