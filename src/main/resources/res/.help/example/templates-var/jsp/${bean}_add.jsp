<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@include file="/jsp/common/common.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>${bean}_add</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="<%=basePath%>style/crm.css" rel="stylesheet"
			type="text/css">
		<style type="text/css"></style>
		<script type="text/javascript" language="javascript"
			src="<%=basePath%>script/public.js">
</script>
		<script type="text/javascript" language="javascript"
			src="<%=basePath%>script/My97DatePicker/WdatePicker.js">
</script>
		<script type="text/javascript" language="javascript">
function submit2() {
#foreach ($field in ${bean.fields})
	#if(${field.isRequired} == true)
	if(checkNullStr(document.getElementById("${bean}.${field}"),"[${field.alias}]字段不能为空!")){
		return;
	}
	#end
	#if(${field.type} == "Integer")
	if (!checkNum(document.getElementById("${bean}.${field}"),"[${field.alias}]字段请输入数字")) {
		return;
	}
	#end
#end
	with (document.getElementById("myform")) {
		submit();
	}
}
</script>
	</head>
	<body>
		<center>
		<font color="red"><s:fielderror/></font>
			<form action="${bean}_add.action" method="post" name="myform"
				id="myform">
				<table class="tableEdit" cellspacing="1" cellpadding="0">
					<tbody>
						<tr>
							<!-- 这里是添加、编辑界面的标题 -->
							<td align="center" class="tdEditTitle">
								添加${bean}信息
							</td>
						</tr>
						<tr>
							<td>
								<!-- 主输入域开始 -->
								<table class="tableEdit" cellspacing="0" cellpadding="0">
									#set($count=1) #foreach ($field in ${bean.fields})
									#if($velocityCount%2!=0)
									<tr>
									#end
										<td class="tdEditLabel">
											${field.alias}
										</td>
										#if(${field.type} == "Date")
											<td class="tdEditContent">
												<input type="text" name="${bean}.${field}"
													id="${bean}.${field}" onfocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})">
											</td>
										#else
											#if(${field.comboLength} >0 )
												<td class="tdEditContent">
													<select name="${bean}.${field}" id="${bean}.${field}">
													#foreach ($str in ${field.combo})
														<option value="${str}">
														${str}
														</option>
													#end
													</select>
												</td>
											#else	
												<td class="tdEditContent">
													<input type="text" name="${bean}.${field}"
														id="${bean}.${field}">
												</td>
											#end
										#end
									#if($velocityCount%2==0)
									</tr>
									#end #set($count = $count + 1) #end #if($count%2==0)
									<td class="tdEditLabel"></td>
									<td class="tdEditContent"></td>
									</tr>
									#end
								</table>
								<!-- 主输入域结束 -->
							</td>
						</tr>
					</tbody>
				</table>

				<table style="width: 100%">
					<tr align="center">
						<td colspan="3">
							<input class="MyButton" type="button" name="saveButton"
								value="保存信息" onclick="submit2();">
							<input class="MyButton" type="button" name="closeButton"
								value="关闭窗口" onclick="window.close();">
						</td>
					</tr>
				</table>
			</form>
		</center>
	</body>
</html>

