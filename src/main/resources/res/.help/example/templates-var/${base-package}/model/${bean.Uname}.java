package ${base-package}.model;

import java.util.*;
import javax.persistence.*;
import com.tgb.lk.util.base.model.BaseTimeModel;
import com.tgb.lk.util.excel.ExcelVOAttribute;

@Entity
@Table(name = "t_${bean}")
public class ${bean.Uname} extends BaseTimeModel{
	
#set ( $arr = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'] )

#foreach ($field in ${bean.fields})
#if(${field.isRequired})
	@Column(name = "${field}", nullable = false)
#else
	@Column(name = "${field}")
#end
	@ExcelVOAttribute(name = "${field.alias}", column = "$arr[$velocityCount]")
	private ${field.type} ${field};

#end

	public ${bean.Uname}() {
	}
	
#foreach ($field in ${bean.fields})
	public ${field.type} get${field.Uname}() {
		return ${field};
	}

	public void set${field.Uname}(${field.type} ${field}) {
		this.${field} = ${field};
	}
#end

}
