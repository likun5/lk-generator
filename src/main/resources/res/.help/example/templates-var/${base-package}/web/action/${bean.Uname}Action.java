package ${base-package}.web.action;

import java.util.*;

import com.tgb.lk.util.base.action.*;
import com.tgb.lk.util.excel.ExcelUtil;
import com.tgb.lk.util.query.*;

import ${base-package}.model.*;
import ${base-package}.service.*;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ${bean.uname}Action extends BaseAction{

	public ${bean.uname}Action() {
	}

	private ${bean.uname} ${bean.name};

	public ${bean.uname} get${bean.uname}() {
		return ${bean.name};
	}

	public void set${bean.uname}(${bean.uname} ${bean.name}) {
		this.${bean.name} = ${bean.name};
	}
	

	private ${bean.uname}Service ${bean.name}Service;

	public void set${bean.uname}Service(${bean.uname}Service ${bean.name}Service) {
		this.${bean.name}Service = ${bean.name}Service;
	}

	public String add() {
		if (${bean.name} != null) {
			${bean.name}Service.save(${bean.name});
		}
		return "add";
	}

	public String showUpdate() {
		if (!id.equals("")) {
			${bean.name} = ${bean.name}Service.find(id);
		}
		return "showUpdate";
	}

	public String update() {
		if (${bean.name} != null) {
			${bean.name}Service.update(${bean.name});
		}
		return "update";
	}

	public String delete() {
		String ids = args.get("ids");
		String[] strIds = ids.split(",");
		${bean.name}Service.delete(strIds);
		return "delete";
	}


	public String list() {
		List<${bean.Uname}> list = new ArrayList<${bean.Uname}>();
		result = ${bean.name}Service.query();
		list = result.getResult();
		return "list";
	}

	public String find() {
		if (!id.equals("")) {
			${bean.name} = ${bean.name}Service.find(id);
		}
		return "find";
	}
	
	public String export() {
		return "export";
	}

	
}
