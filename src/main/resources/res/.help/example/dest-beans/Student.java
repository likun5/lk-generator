import java.util.*;
import com.tgb.lk.annotation.*;

@AutoBean(alias = "Student",table="T_student")
public class Student {

  @AutoField(alias = "序号", column = "id", isKey = true , isRequired = true , type="String")
  @ExcelVOAttribute(name = "id", column = "A")
  private String id;

  @AutoField(alias = "姓名", column = "name", isRequired = true)
  @ExcelVOAttribute(name = "name", column = "B")
  private String name;

  @AutoField(alias = "性别", column = "sex", combo = {"男","女"})
  @ExcelVOAttribute(name = "sex", column = "C")
  private String sex;

  @AutoField(alias = "年龄", column = "age", type = "Integer")
  @ExcelVOAttribute(name = "age", column = "D")
  private int age;

  @AutoField(alias = "生日", column = "birthday", type = "Date")
  @ExcelVOAttribute(name = "birthday", column = "E")
  private Date birthday;

  @AutoField(alias = "是否班长", column = "is_monitor")
  @ExcelVOAttribute(name = "is_monitor", column = "F")
  private Boolean isMonitor;

  @AutoField(alias = "创建时间", column = "create_time", isRequired = true, type = "Date")
  @ExcelVOAttribute(name = "create_time", column = "G")
  private Date createTime;

  @AutoField(alias = "修改时间", column = "update_time", isRequired = true, type = "Date")
  @ExcelVOAttribute(name = "update_time", column = "H")
  private Date updateTime;


  public String getId() {
    return id;
  }
  public void setId(String id){
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name){
    this.name = name;
  }
  public String getSex() {
    return sex;
  }
  public void setSex(String sex){
    this.sex = sex;
  }
  public int getAge() {
    return age;
  }
  public void setAge(int age){
    this.age = age;
  }
  public Date getBirthday() {
    return birthday;
  }
  public void setBirthday(Date birthday){
    this.birthday = birthday;
  }
  public Boolean getIsMonitor() {
    return isMonitor;
  }
  public void setIsMonitor(Boolean isMonitor){
    this.isMonitor = isMonitor;
  }
  public Date getCreateTime() {
    return createTime;
  }
  public void setCreateTime(Date createTime){
    this.createTime = createTime;
  }
  public Date getUpdateTime() {
    return updateTime;
  }
  public void setUpdateTime(Date updateTime){
    this.updateTime = updateTime;
  }
}
