欢迎访问我的博客关注本代码生成器的最新动态： http://blog.csdn.net/lk_blog


代码生成工具使用前置条件：系统安装JDK并配置JAVA_HOME环境变量。

功能：
一、数据库生成实体类.
1.配置
（1）本工具提供了3中连接数据库的方式，分别是：MySql、Oracle、SqlServer，在界面上给出连接示例，方便进行复制和修改（减少因错误配置连接方式而带来了不能链接数据库问题）。配置正确后即可正确读取出数据库中的相关表和表中字段。

（2）生成的代码路径可根据需要进行灵活配置，如果没有配置默认为：d:\.LKGenerator\dest-beans

（3）提供了根据数据表生成代码的相关配置，可以配置去掉表前缀（如t_）,去掉表后缀，去掉表名内容下划线（业界习惯表命名中用下划线区分单词），大写表名下划线后的第一个字母，去掉属性中下划线并大写其后第一个字母，这样可以使生成的代码更符合业界代码命名习惯，如，用户和角色的关联关系表常被命名为：t_user_role，表中字段有user_id和role_id，按上面的配置后生成的代码为UserRole.java，类中的属性为userId和roleId.

2.选择表
前置条件：本功能要求第一步配置数据库连接方式能正确连接到数据库。
本功能可列出所配置数据库的所有表，请选择所需生成代码的表后点击确认按钮（注：如果重新配置步骤1的数据库连接后需刷新列表）。

3.生成实体类代码
前置条件：步骤2中选择了需要生成代码的表。
本功能可根据步骤2中选择表进行实体Beans代码的生成，生成的代码默认包含AutoBean和AutoField的注解配置（AutoBean和AutoField注解非生成代码的必须项，如果配置了可以在生成代码时时为模版注入该配置，增强模版的扩展性）。

4.编译实体类
前置条件：系统安装JDK并配置JAVA_HOME环境变量
本功能可以对Java代码进行编译（注：本功能的使用不必要依赖步骤3中的代码，可对任何正确的java代码进行编译，如果代码中有引用第三方的jar，请将jar放置到d:\.LKGenerator\.lib文件夹下。）

5.导出数据到Excel
前置条件：执行第三步后已生成带注解的JavaBean.
本功能只要使用地三步的配置注解的JavaBean就能将数据表中的数据导出。

6.从Excel导入到DB
前置条件：执行第三步后已生成带注解的JavaBean.
本功能只要使用地三步的配置注解的JavaBean就能将Excel中的数据导入到数据表中。

7.生成数据表文档
前置条件：步骤2中选择了需要生成文档的表。
本功能可以根据步骤2中选择的表进行excel文档的生成。

8.清空Beans代码目录。
本功能可以清空生成代码的文件夹，主要为了增强用户体验，让使用者方便打开Beans文件夹并对代码进行修改。

9.打开Beans代码目录
本功能可以打开生成代码的文件夹，主要为了增强用户体验，让使用者方便打开Beans文件夹并对代码进行修改。

二、实体类根据模版生成最终代码.
功能简介：根据实体类代码结合模版生成最终代码,代码模版规范参考模版框架velocity的规范和样例。
实现思路：加载实体类的class文件到classLoader中，读取实体类的属性和注解，将类名、属性字段名、注解三者与模版进行结合生成最终的代码。

1.配置
（1）配置模版路径，这个配置可以配置用于生成代码的velocity模版路径，生成的代码将模版中的特定符号见“模版中可使用的变量”中的描述。

（2）配置生成代码的固定文件路径，这个配置中的文件将原样拷贝到最终生成的代码中，主要适用于非文本文件如*.jpg *.jar等文件。

（3）配置引用jar路径，这个jar路径的配置是需要加载的class需要引用的jar.

（4）注入模版的Bean(*.class)路径，这个路径下需放置实体Bean的class文件。

（5）配置生成代码的根路径，这个路径即最终生成代码的路径。

（6）配置注入模板的键值，这个功能可以配置除默认注入到模版的变量之外的特殊变量，例:author=李坤,模板可用变量${author},base-package=com.tgb.lk,则模版中可使用${base-package}

（7）读取数据库中的数据用于生成代码。
本配置主要控制是否开启读取配置数据库中数据后作为模板的输入功能。开启本功能将自动将数据库中的数据转换成JavaBean并将此JavaBean作为模板变量的输入用于生成代码。


模板中可使用的通用变量：
${base-package}包名的默认变量,可在步骤(5)中进行属性值的设置,可以放到路径中,路径中使用时将把配置的点(.)转为路径分隔符(/)使用.

${beans}实体类的类名集合.

${bean}实体类类名首字母变为小写.
${bean.Lname}同${bean},实体类类名首字母变为小写.
${bean.Uname}实体类类名首字母变为大写.
${bean.ALname}实体类类名全部变为小写字母.
${bean.AUname}实体类类名全部变为大写字母.

注:${bean},${bean.Lname},${bean.Uname},${bean.ALname},${bean.AUname},${base-package}可以用于模版中也可以用于路径和文件的命名.


注解增强Bean属性,需配置@AutoBean(alias="xxx",table="xxx",args={"xxx","yyy"}):
${bean.alias}实体类注解中的alias值,如果实体类上没有配置,则默认为类名.可配置中文注释,生成代码时显示该注释.
${bean.table}实体类注解中的table值,对应数据库中的表名.
${bean.argsLength},${bean.argsSize}bean注解中的args参数数组配置的参数个数.
${bean.args[0]}bean注解中的args参数数组配置中的第一个值.
${bean.fieldsLength},${bean.fieldsSize}bean中包含的字段的个数.


${bean.fields}实体类的字段集合,可对其进行循环.
${field}实体类属性名.

${field}实体属性首字母变为小写.
${field.Lname}同${bean},实体类属性首字母变为小写.
${field.Uname}实体类属性首字母变为大写.
${field.ALname}实体类属性全部变为小写字母.
${field.AUname}实体类属性全部变为大写字母.


注解增强Field属性,需配置  @AutoField(alias = "序号", column = "ID", columnType="varchar", isKey = true , isRequired = true , type="String", combo = {"男","女"},args={"xxx","yyy"})
${field.alias}实体属性注解中的alias值,默认为属性名.可配置中文注释,生成代码时显示该注释.
${field.column}实体属性注解中的column值,数据库列名,支持Lcolumn,Ucolumn,ALcolumn,AUcolumn
${field.columnType}实体属性注解中的columnType值,数据库列的类型.
${field.isKey}实体属性注解中的isKey值,boolean类型,配置后可以使用${bean.key}变量.
${field.isRequired}实体属性中的isRequired属性,该属性可以配置该字段是否为必填项.
${field.type}实体类属注解中的类型,例如String,Integer...
${field.comboLength},${field.comboSize}实体属性注解中combo参数数组配置的参数个数.
${field.combo[0]}实体属性注解中combo参数数组配置中的第一个值.
${field.argsLength}}实体属性注解中的args参数数组配置的参数个数.
${field.args[0]}实体属性注解中args参数数组配置中的第一个值.

${bean.datasLength} = ${bean.datasSize}bean中读取数据表中的内容并转为类的对象,模板中可以使用.
#foreach($data in ${bean.datas})
   ${data.uuid},${data.name}...
#end

2.从实体类生成代码
前置条件：步骤1中的（1）中配置了可用的velocity模版，步骤1中（4）中有可用的*.class文件。
本功能用于生成代码。

3.打开生成代码目录
本功能可以打开生成代码的文件夹，主要为了增强用户体验，让使用者方便打开Beans文件夹并对代码进行修改。


其他:
1.字段转换对应关系:
public static final String DATA_TYPE[] = {"Integer", "Double", "String", "Date", "Boolean", "Long",
            "Float", "byte[]", "BigInteger", "BigDecimal"};
 //oracle
 _TYPE_MAP.put("bit", Integer.valueOf(4));
 _TYPE_MAP.put("raw", Integer.valueOf(7));
 _TYPE_MAP.put("char", Integer.valueOf(2));
 _TYPE_MAP.put("varchar2", Integer.valueOf(2));
 _TYPE_MAP.put("number", Integer.valueOf(0));
 _TYPE_MAP.put("numberf", Integer.valueOf(1));
 _TYPE_MAP.put("longtext", Integer.valueOf(2));
 _TYPE_MAP.put("integer", Integer.valueOf(0));
 _TYPE_MAP.put("blob", Integer.valueOf(7));
 _TYPE_MAP.put("clob", Integer.valueOf(7));
 _TYPE_MAP.put("nclob", Integer.valueOf(7));
 _TYPE_MAP.put("timestamp", Integer.valueOf(3));
 _TYPE_MAP.put("date", Integer.valueOf(3));

 //sqlServer,参考http://blog.csdn.net/lg312200538/article/details/5993049
 _TYPE_MAP.put("int", Integer.valueOf(0));
 _TYPE_MAP.put("varchar", Integer.valueOf(2));
 _TYPE_MAP.put("char", Integer.valueOf(2));
 _TYPE_MAP.put("nchar", Integer.valueOf(2));
 _TYPE_MAP.put("nvarchar", Integer.valueOf(2));
 _TYPE_MAP.put("text", Integer.valueOf(2));
 _TYPE_MAP.put("ntext", Integer.valueOf(2));
 _TYPE_MAP.put("tinyint", Integer.valueOf(0));
 _TYPE_MAP.put("int", Integer.valueOf(0));
 _TYPE_MAP.put("tinyint", Integer.valueOf(0));
 _TYPE_MAP.put("smallint", Integer.valueOf(0));
 _TYPE_MAP.put("bit", Integer.valueOf(4));
 _TYPE_MAP.put("bigint", Integer.valueOf(5));
 _TYPE_MAP.put("float", Integer.valueOf(6));
 _TYPE_MAP.put("decimal", Integer.valueOf(9));
 _TYPE_MAP.put("money", Integer.valueOf(9));
 _TYPE_MAP.put("smallmoney", Integer.valueOf(9));
 _TYPE_MAP.put("numeric", Integer.valueOf(9));
 _TYPE_MAP.put("real", Integer.valueOf(6));
 _TYPE_MAP.put("smalldatetime", Integer.valueOf(3));
 _TYPE_MAP.put("datetime", Integer.valueOf(3));
 _TYPE_MAP.put("timestamp", Integer.valueOf(3));
 _TYPE_MAP.put("binary", Integer.valueOf(7));
 _TYPE_MAP.put("varbinary", Integer.valueOf(7));
 _TYPE_MAP.put("image", Integer.valueOf(7));


 //mysql对应关系表,参考http://www.cnblogs.com/jerrylz/p/5814460.html整理
 _TYPE_MAP.put("varchar", Integer.valueOf(2));
 _TYPE_MAP.put("char", Integer.valueOf(2));
 _TYPE_MAP.put("blob", Integer.valueOf(7));
 _TYPE_MAP.put("text", Integer.valueOf(2));
 _TYPE_MAP.put("int", Integer.valueOf(0));
 _TYPE_MAP.put("integer", Integer.valueOf(0));
 _TYPE_MAP.put("tinyint", Integer.valueOf(0));
 _TYPE_MAP.put("smallint", Integer.valueOf(0));
 _TYPE_MAP.put("mediumint", Integer.valueOf(0));
 _TYPE_MAP.put("bit", Integer.valueOf(4));
 _TYPE_MAP.put("bigint", Integer.valueOf(5));
 _TYPE_MAP.put("float", Integer.valueOf(6));
 _TYPE_MAP.put("double", Integer.valueOf(1));
 _TYPE_MAP.put("decimal", Integer.valueOf(9));
 _TYPE_MAP.put("boolean", Integer.valueOf(4));
 _TYPE_MAP.put("date", Integer.valueOf(3));
 _TYPE_MAP.put("time", Integer.valueOf(3));
 _TYPE_MAP.put("datetime", Integer.valueOf(3));
 _TYPE_MAP.put("timestamp", Integer.valueOf(3));

